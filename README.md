# Arlang
## An interpreter for a programming language with dynamic size arrays
Made for a Compilation Techniques course as the Warsaw University of Technology

### Interpreter instalation
You need to have the [Rust toolchain installed](https://www.rust-lang.org/tools/install).\
Then install to the cargo bin directory (normally `~/.cargo/bin/arlang`)
```
cargo install --path .
```
or skip and use cargo to run the project instead of installing it.
### Running the interpreter
```
arlang [FLAGS] [source]
cargo run -- [FLAGS] [source]
```
### Show help
```
arlang --help
cargo run -- --help
```
### Running tests
```
cargo test
```
### Generating internal code documentation
```
cargo doc --document-private-items
```
Docs are generated under [target/doc/](target/doc/arlang/index.html)


# Language description
### Features:
- static typing with implicit variable typing
- dynamic size arrays
- nice error messages with code highlighting
### Program structure:
- the program consists of a list of function definitions
- `main()` function is the entry point of the program
### Functions:
- definition - `fn name(argument: Type, ...) { statement; ... }`
- by default return `Unit`, can be changed with `: Type` behind the argument list
- if the function does not return `Unit`, the last instruction of the function has to be a `return` or a call to `panic`
### Instructions:
- variable declaration
  - `let name: Type = value;`
  - providing the variable type is optional - the type gest inferred from the initial value
- assignment
  - `variable = expression;`
  - `array[index] = expression;`
- return from function
  - `return value;`
- `if` conditional
  - `if condition {...} elif condition {...} else {...}`
- `while` loop
  - `while condition {...}`
- expression followed by `;`
  - e.g. `2+2;`
  - e.g. `function_call();`
### Types:
- Simple types:
  - `Unit`, `Int`, `Float`, `Bool`, `String`
  - value passed by copy
- Array types:
  - `[Unit]`, `[Int]`, `[Float]`, `[Bool]`, `[String]`
  - value passed by reference
### Operators:
- logical
  - (`!`, `&&`, `||`) - defined for `Bool`
- comparisons
  - (`<`, `<=`, `>`, `>=`) - defined for `Int` and `Float`
  - (`==`, `!=`) - defined for everything except `Unit`
- arithmetic
  - (`+`) - defined for `Int`, `Float`, `String` and any array
  - (`-`, `*`, `/`) - defined `Int` and `Float`
- `as` casts
  - `Int` -> `Float` | `String`
  - `Float` -> `Int` | `String`
  - `Bool` -> `Int` | `Float` | `String`
  - `T` -> `T` (identity cast)
### Value creation:
- `Unit` - `()`
- `Int` - integer literal (e.g. `0`, `1`, `5321`)
- `Float` - float literal (e.g. `0.0`, `0.1`, `123.45`)
- `Bool` - `true` or `false`
- `String`:
  - characters between `""` (e.g. `"hello world"`)
  - the literal can contain any single character except `\` and `"`, and character escape sequences `\\`, `\n` and `\"`
- tables (`[T]`):
  - value list - `[x, y, z, ...]`
  - repeat value x n times - `[x; n]`
  - empty array - `[x; 0]` - forces the expression to have a specific type
### Built-in functions:
  - `print(s: String)` - prints the given string to stdout
  - `panic()` - terminates the program with error
  - `size(array: [T]): Int` - returns the size of an array
  - `push(array: [T], value: T)` - adds a value to the end of an array
  - `pop(array: [T])` - removes the last element of an array
  - `insert(array: [T], index: Int, value: T)` - inserts a value at an index in an array
  - `remove(array: [T], index: Int)` - removes an element at an index in an array
### Keywords
- `fn`, `let`, `as`, `if`, `while`, `return`, `true`, `false`, `Unit`, `Int`, `Float`, `Bool`, `String`
### Comments
- skipped by the tokenizer
- between `//` and end of line (outside of string literals)


# [Example code](example.ar)
```
// Arlang example

fn main() {
    let x: Int = 1 + 2 * 3;
    let y = 1.0 + x as Float;
    print(y as String + " <- Float\n");
    // 8 <- Float

    let arr = [""; 1];
    arr[0] = "Hello";
    push(arr, "!");
    insert(arr, 1, "World");
    print_string_array(arr);
    // Hello World !

    print(
        sum_int_array([1, 2, 3, 4, 5]) as String + "\n",
    );
    // 15

    let arr = [1, 2, 0, 4, 5];
    set_third_int_value(arr, 3);
    remove(arr, 1);
    pop(arr);
    print_string_array(
        int_array_to_string_array(arr),
    );
    // 1 3 4

    print(power(2.0, 8) as String + "\n");
    // 256

    print_string_array(
        int_array_to_string_array(
            fibbonaci(10),
        ),
    );
    // 1 1 2 3 5 8 13 21 34 55
}

fn print_string_array(arr: [String]) {
    let i = 0;
    while i < size(arr) {
        print(arr[i] as String);
        if i < size(arr) - 1 {
            print(" ");
        } else {
            print("\n");
        }
        i = i + 1;
    }
}

fn sum_int_array(arr: [Int]): Int {
    let i = 0;
    let sum = 0;
    while i < size(arr) {
        sum = sum + arr[i];
        i = i + 1;
    }
    return sum;
}

fn set_third_int_value(arr: [Int], value: Int) {
    arr[2] = value;
}

fn int_array_to_string_array(arr: [Int]): [String] {
    let i = 0;
    let sarr = [""; 0];
    while i < size(arr) {
        insert(sarr, i, arr[i] as String);
        i = i + 1;
    }
    return sarr;
}

fn power(base: Float, exponent: Int): Float {
    if exponent == 0 {
        return 1.0;
    }
    let temp = power(base, exponent / 2);
    if mod(exponent, 2) == 0 {
        return temp * temp;
    } else {
        if exponent > 0 {
            return base * temp * temp;
        } else {
            return temp * temp / base;
        }
    }
    panic(); // unreachable
}

fn mod(number: Int, divisor: Int): Int {
    return number - divisor * (number / divisor);
}

fn fibbonaci(n: Int): [Int] {
    if n <= 0 { return [0; 0]; }
    if n == 1 { return [1]; }
    let arr = [1, 1];
    let i = 2;
    while i < n {
        push(arr, arr[i - 1] + arr[i - 2]);
        i = i + 1;
    }
    return arr;
}
```
### Example code output
```
8 <- Float
Hello World !
15
1 3 4
256
1 1 2 3 5 8 13 21 34 55

```


# Language grammar
### Primitives
```
simple type =  "Unit"  |  "Int"  |  "Float"  |  "Bool"  |  "String";

letter = "A" | "B" | "C" | "D" | "E" | "F" | "G"
       | "H" | "I" | "J" | "K" | "L" | "M" | "N"
       | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
       | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z" ;

digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";

bool = "true" | "false";

unit = "(", ")";

integer = digit, {digit};

float = digit, {digit}, ".", digit, {digit};

string = '"', { ? anychar without " and \ ? | '\n' | '\\' | '\"' }, '"';

Type = simple type | "[", simple type, "]"

Literal = integer | float | string | bool | unit;

Identifier = (letter | "_") , {letter | digit | "_"};
```
### Syntax
```
Program = {Function};

Function = "fn", Identifier, Parameters, [":", Type], Body;

Parameters = "(", [Parameter, {",", Parameter}, [","]], ")";

Parameter = Identifier, ":", Type;

Body = "{", {Statement}, "}";

Statement = Declaration | Return | If | While | Expression, ";";

If = "if", Expression, Body, {"elif", Expression, Body}, ["else", Body];

While = "while", Expression, Body;

Declaration = "let", Identifier, [":", Type], "=", Expression, ";";

Return = "return", Expression, ";";

Expression = Expr0;
Expr0 = Expr1, {"=", Expr1};
Expr1 = Expr2, {"||", Expr2};
Expr2 = Expr3, {"&&", Expr3};
Expr3 = Expr4, {"<" | ">" | "<=" | ">=" | "==" | "!=", Expr4};
Expr4 = Expr5, {"+" | "-", Expr5};
Expr5 = Expr6, {"*" | "/", Expr6};
Expr6 = Expr7, {"as", Type};
Expr7 = {"-" | "!"}, Expr7;
Expr8 = Expr9, ['[', Expr0, ']'];
Expr9 = Identifier | Call | Literal | ArrayInit | "(", Expr0, ")";

Call = Identifier, "(", [Expression, {",", Expression}, [","]] ")";

ArrayInit = ArrayList | ArrayRepeat;

ArrayList = "[", Expression, {",", Expression}, [","], "]";

ArrayRepeat = "[", Expression, ";", Expression, "]";
```
