use std::{error::Error, fs::File, io::Read, path::PathBuf};
use structopt::StructOpt;

use analyzer::SemanticAnalyzer;
use interpreter::Interpreter;
use parser::Parser;
use source::{Source, SourceError, SourceIter};
use tokenizer::Tokenizer;

mod analyzer;
mod common;
mod interpreter;
mod parser;
mod source;
mod tokenizer;

#[derive(Debug, StructOpt)]
/// Interpreter of the arlang language.
struct Options {
    /// Stream input.
    ///
    /// Streams the text input instead of loading the whole source into memory.
    /// Makes error messages less nice.
    #[structopt[long]]
    stream: bool,

    /// Analyze functions in parallel.
    ///
    /// If provided, the semantic analyzer will analyze functions in parallel.
    /// Functions would have to be absurdly large for this to improve performance.
    #[structopt[short, long]]
    parallel: bool,

    /// Path to source code.
    ///
    /// If provided, code from the given file will be interpreted.
    /// Otherwise code is read from standard input.
    #[structopt(parse(from_os_str))]
    source: Option<PathBuf>,
}

fn main() {
    let res = real_main();
    if res != 0 {
        std::process::exit(res)
    }
}

fn real_main() -> i32 {
    let options = Options::from_args();
    let result = if options.stream {
        run_without_source(&options)
    } else {
        run_with_source(&options)
    };
    if let Err(e) = &result {
        eprintln!("{}", e);
        return 1;
    }
    0
}

fn run_without_source(options: &Options) -> Result<i32, Box<dyn Error>> {
    let interpreter_result = match &options.source {
        Some(path) => {
            let source_iter = SourceIter::new(File::open(path)?);
            run_interpreter(source_iter, options.parallel)
        }
        None => {
            let source_iter = SourceIter::new(std::io::stdin());
            run_interpreter(source_iter, options.parallel)
        }
    };
    if let Err(e) = interpreter_result {
        let name = match &options.source {
            Some(path) => format!("{:?}", path),
            None => "stdin".to_string(),
        };
        eprint!("{}", e.display_no_src(&name));
        Ok(1)
    } else {
        Ok(0)
    }
}

fn run_with_source(options: &Options) -> Result<i32, Box<dyn Error>> {
    let mut src = Vec::new();
    match &options.source {
        Some(path) => File::open(path)?.read_to_end(&mut src)?,
        None => std::io::stdin().read_to_end(&mut src)?,
    };
    let source_iter = SourceIter::new(src.as_slice());
    if let Err(e) = run_interpreter(source_iter, options.parallel) {
        let source = Source::new(src.as_slice())?;
        eprint!("{}", e.display(&source));
        Ok(1)
    } else {
        Ok(0)
    }
}

fn run_interpreter<S: Read>(
    source_iter: SourceIter<S>,
    parallel_ananalysis: bool,
) -> Result<(), SourceError> {
    let tokenizer = Tokenizer::new(source_iter);
    let parser = Parser::new(tokenizer);
    let syntax_tree = parser.parse()?;
    let semantic_analyzer = SemanticAnalyzer::new(syntax_tree, parallel_ananalysis);
    let ircode = semantic_analyzer.analyze()?;
    let interpreter = Interpreter::new(&ircode, std::io::stdout());
    interpreter.run()?;
    Ok(())
}
