//! Source code tokenization.

use std::{
    io::Read,
    iter::Peekable,
    num::IntErrorKind,
    ops::{ControlFlow, Range},
};

use crate::{
    source::{SourceError, SourceIndex, SourceIter, SourceIterItem},
    tokenizer::token::{Operator, Symbol},
};

use self::{token::prelude::*, token_check::TokenCharCheck};

pub mod token;
mod token_check;

/// Tokenizer object.
/// Iterator over [TokenizerOutput].
/// After yielding and error always yields None.
#[derive(Debug)]
pub struct Tokenizer<S: Read> {
    src_iter: Peekable<SourceIter<S>>,
    error: bool,
}

impl<S: Read> Tokenizer<S> {
    pub fn new(src_iter: SourceIter<S>) -> Self {
        Self {
            src_iter: src_iter.peekable(),
            error: false,
        }
    }

    fn next_token(&mut self) -> Option<TokenizerOutput> {
        if self.error {
            None
        } else {
            loop {
                match self.start_parse() {
                    ControlFlow::Continue(()) => continue,
                    ControlFlow::Break(output) => {
                        if let Some(Err(_)) = &output {
                            self.error = true;
                        }
                        break output;
                    }
                }
            }
        }
    }

    fn start_parse(&mut self) -> ControlFlow<Option<TokenizerOutput>> {
        self.skip_whitespace();
        match self.src_iter.next() {
            None => ControlFlow::Break(None),
            Some(item) => match item.char {
                c if c.is_identifier_start() => {
                    ControlFlow::Break(Some(Ok(self.parse_identifier_type_keyword_bool_or_as(
                        TokenBuffer::new(Some(item.char), item.index),
                    ))))
                }
                c if c.is_digit(10) => ControlFlow::Break(Some(
                    self.parse_number_literal(TokenBuffer::new(Some(item.char), item.index)),
                )),
                c if c.is_operator_start() => {
                    self.parse_operator_or_comment(TokenBuffer::new(None, item.index), item.char)
                }
                c if c.is_syntax_symbol() => ControlFlow::Break(Some(Ok(
                    self.parse_syntax_symbol(TokenBuffer::new(None, item.index), item.char)
                ))),
                c if c == '"' => ControlFlow::Break(Some(
                    self.parse_string_literal(TokenBuffer::new(None, item.index)),
                )),
                _ => ControlFlow::Break(Some(Err(TokenizerError::UnexpectedTokenStart(
                    item.index, item.char,
                )))),
            },
        }
    }

    fn parse_identifier_type_keyword_bool_or_as(
        &mut self,
        mut buffer: TokenBuffer,
    ) -> TokenizerItem {
        let mut last_index = buffer.start;
        while self.src_iter.peek().is_some()
            && self
                .src_iter
                .peek()
                .unwrap()
                .char
                .is_identifier_continuation()
        {
            let next = self.src_iter.next().unwrap();
            buffer.text.push(next.char);
            last_index = next.index;
        }
        match buffer.text.as_str() {
            "Unit" => TokenizerItem {
                token: Token::Type(UnitT),
                span: buffer.start..last_index.next(),
            },
            "Int" => TokenizerItem {
                token: Token::Type(IntT),
                span: buffer.start..last_index.next(),
            },
            "Float" => TokenizerItem {
                token: Token::Type(FloatT),
                span: buffer.start..last_index.next(),
            },
            "Bool" => TokenizerItem {
                token: Token::Type(BoolT),
                span: buffer.start..last_index.next(),
            },
            "String" => TokenizerItem {
                token: Token::Type(StringT),
                span: buffer.start..last_index.next(),
            },
            "fn" => TokenizerItem {
                token: Token::Keyword(Fn),
                span: buffer.start..last_index.next(),
            },
            "let" => TokenizerItem {
                token: Token::Keyword(Let),
                span: buffer.start..last_index.next(),
            },
            "if" => TokenizerItem {
                token: Token::Keyword(If),
                span: buffer.start..last_index.next(),
            },
            "elif" => TokenizerItem {
                token: Token::Keyword(Elif),
                span: buffer.start..last_index.next(),
            },
            "else" => TokenizerItem {
                token: Token::Keyword(Else),
                span: buffer.start..last_index.next(),
            },
            "while" => TokenizerItem {
                token: Token::Keyword(While),
                span: buffer.start..last_index.next(),
            },
            "return" => TokenizerItem {
                token: Token::Keyword(Return),
                span: buffer.start..last_index.next(),
            },
            "as" => TokenizerItem {
                token: Token::Operator(As),
                span: buffer.start..last_index.next(),
            },
            "true" => TokenizerItem {
                token: Token::Literal(BoolL(true)),
                span: buffer.start..last_index.next(),
            },
            "false" => TokenizerItem {
                token: Token::Literal(BoolL(false)),
                span: buffer.start..last_index.next(),
            },
            _ => TokenizerItem {
                token: Token::Identifier(buffer.text),
                span: buffer.start..last_index.next(),
            },
        }
    }

    fn parse_number_literal(&mut self, mut buffer: TokenBuffer) -> TokenizerOutput {
        let mut last_index = buffer.start;
        while self.src_iter.peek().is_some() && self.src_iter.peek().unwrap().char.is_digit(10) {
            let next = self.src_iter.next().unwrap();
            buffer.text.push(next.char);
            last_index = next.index;
        }
        if let Some(SourceIterItem { char: '.', .. }) = self.src_iter.peek() {
            let next = self.src_iter.next().unwrap();
            buffer.text.push(next.char);
            last_index = next.index;
            self.parse_float_literal_after_dot(buffer, last_index)
        } else {
            match buffer.text.parse() {
                Ok(int) => Ok(TokenizerItem {
                    token: Token::Literal(IntL(int)),
                    span: buffer.start..last_index.next(),
                }),
                Err(error) => {
                    if let IntErrorKind::PosOverflow = error.kind() {
                        Err(TokenizerError::IntLiteralOutOfRange(
                            buffer.start..last_index.next(),
                        ))
                    } else {
                        unreachable!("Parsed invalid int literal")
                    }
                }
            }
        }
    }

    fn parse_float_literal_after_dot(
        &mut self,
        mut buffer: TokenBuffer,
        mut last_index: SourceIndex,
    ) -> TokenizerOutput {
        while self.src_iter.peek().is_some() && self.src_iter.peek().unwrap().char.is_digit(10) {
            let next = self.src_iter.next().unwrap();
            buffer.text.push(next.char);
            last_index = next.index;
        }
        if buffer.text.ends_with('.') {
            Err(TokenizerError::UnfinishedFloatLiteral(
                buffer.start..last_index.next(),
            ))
        } else {
            Ok(TokenizerItem {
                token: Token::Literal(FloatL(buffer.text.parse().unwrap())),
                span: buffer.start..last_index.next(),
            })
        }
    }

    fn parse_operator_or_comment(
        &mut self,
        buffer: TokenBuffer,
        char: char,
    ) -> ControlFlow<Option<TokenizerOutput>> {
        match (char, self.src_iter.peek()) {
            ('/', Some(s)) if s.char == '/' => {
                self.skip_comment();
                ControlFlow::Continue(())
            }
            _ => ControlFlow::Break(Some(self.parse_operator(buffer, char))),
        }
    }

    fn parse_syntax_symbol(&mut self, buffer: TokenBuffer, char: char) -> TokenizerItem {
        match char {
            ',' => TokenizerItem {
                token: Token::Symbol(Symbol::Comma),
                span: buffer.start.span_one(),
            },
            ':' => TokenizerItem {
                token: Token::Symbol(Symbol::Colon),
                span: buffer.start.span_one(),
            },
            ';' => TokenizerItem {
                token: Token::Symbol(Symbol::Semicolon),
                span: buffer.start.span_one(),
            },
            '{' => TokenizerItem {
                token: Token::Symbol(Symbol::LBrace),
                span: buffer.start.span_one(),
            },
            '}' => TokenizerItem {
                token: Token::Symbol(Symbol::RBrace),
                span: buffer.start.span_one(),
            },
            '[' => TokenizerItem {
                token: Token::Symbol(Symbol::LBracket),
                span: buffer.start.span_one(),
            },
            ']' => TokenizerItem {
                token: Token::Symbol(Symbol::RBracket),
                span: buffer.start.span_one(),
            },
            '(' => TokenizerItem {
                token: Token::Symbol(Symbol::LParen),
                span: buffer.start.span_one(),
            },
            ')' => TokenizerItem {
                token: Token::Symbol(Symbol::RParen),
                span: buffer.start.span_one(),
            },
            _ => unreachable!(
                "parse_syntax_symbol_or_unit called with invalid first char: {}",
                char
            ),
        }
    }

    fn parse_string_literal(&mut self, mut buffer: TokenBuffer) -> TokenizerOutput {
        let mut last_index = buffer.start;
        loop {
            let next = self.src_iter.next();
            if let Some(s) = &next {
                last_index = s.index;
            }
            match next.as_ref().map(|x| x.char) {
                None => {
                    return Err(TokenizerError::StringNotClosed(
                        buffer.start..last_index.next(),
                    ))
                }
                Some('"') => {
                    return Ok(TokenizerItem {
                        token: Token::Literal(StringL(buffer.text)),
                        span: buffer.start..last_index.next(),
                    })
                }
                Some('\\') => {
                    let second = self.src_iter.next();
                    if let Some(s) = &second {
                        last_index = s.index;
                    }
                    match second.as_ref().map(|x| x.char) {
                        None => {
                            return Err(TokenizerError::StringNotClosed(
                                buffer.start..last_index.next(),
                            ))
                        }
                        Some('\\') => {
                            buffer.text.push('\\');
                        }
                        Some('n') => {
                            buffer.text.push('\n');
                        }
                        Some('"') => {
                            buffer.text.push('"');
                        }
                        Some(c) => {
                            return Err(TokenizerError::StringInvalidEscape(
                                next.unwrap().index..second.unwrap().index.next(),
                                c,
                            ))
                        }
                    }
                }
                Some(c) => {
                    buffer.text.push(c);
                }
            };
        }
    }

    fn parse_operator(&mut self, buffer: TokenBuffer, char: char) -> TokenizerOutput {
        match char {
            '+' => Ok(TokenizerItem {
                token: Token::Operator(Operator::Plus),
                span: buffer.start.span_one(),
            }),
            '-' => Ok(TokenizerItem {
                token: Token::Operator(Operator::Minus),
                span: buffer.start.span_one(),
            }),
            '*' => Ok(TokenizerItem {
                token: Token::Operator(Operator::Multiply),
                span: buffer.start.span_one(),
            }),
            '/' => Ok(TokenizerItem {
                token: Token::Operator(Operator::Divide),
                span: buffer.start.span_one(),
            }),
            '<' => self.continue_parse_potencial_comparison_operator(
                Operator::Less,
                Operator::LessEqual,
                buffer.start,
            ),
            '>' => self.continue_parse_potencial_comparison_operator(
                Operator::More,
                Operator::MoreEqual,
                buffer.start,
            ),
            '=' => self.continue_parse_potencial_comparison_operator(
                Operator::Assignment,
                Operator::Equal,
                buffer.start,
            ),
            '!' => self.continue_parse_potencial_comparison_operator(
                Operator::Not,
                Operator::NotEqual,
                buffer.start,
            ),
            '&' => {
                if let Some(SourceIterItem { char: '&', .. }) = self.src_iter.peek() {
                    let next = self.src_iter.next().unwrap();
                    Ok(TokenizerItem {
                        token: Token::Operator(Operator::And),
                        span: buffer.start..next.index.next(),
                    })
                } else {
                    Err(TokenizerError::UnfinishedAndOperator(buffer.start))
                }
            }
            '|' => {
                if let Some(SourceIterItem { char: '|', .. }) = self.src_iter.peek() {
                    let next = self.src_iter.next().unwrap();
                    Ok(TokenizerItem {
                        token: Token::Operator(Operator::Or),
                        span: buffer.start..next.index.next(),
                    })
                } else {
                    Err(TokenizerError::UnfinishedOrOperator(buffer.start))
                }
            }
            _ => unreachable!("parse_operator called with invalid first char: {}", char),
        }
    }

    fn continue_parse_potencial_comparison_operator(
        &mut self,
        if_end: Operator,
        if_with_equals: Operator,
        span_start: SourceIndex,
    ) -> TokenizerOutput {
        if let Some(SourceIterItem { char: '=', .. }) = self.src_iter.peek() {
            let next = self.src_iter.next().unwrap();
            Ok(TokenizerItem {
                token: Token::Operator(if_with_equals),
                span: span_start..next.index.next(),
            })
        } else {
            Ok(TokenizerItem {
                token: Token::Operator(if_end),
                span: span_start.span_one(),
            })
        }
    }

    fn skip_comment(&mut self) {
        while self.src_iter.peek().is_some() && self.src_iter.peek().unwrap().char != '\n' {
            self.src_iter.next();
        }
    }

    fn skip_whitespace(&mut self) {
        loop {
            match &self.src_iter.peek() {
                Some(peek) if peek.char.is_whitespace() => {
                    self.src_iter.next();
                }
                _ => return,
            }
        }
    }
}

impl<S: Read> Iterator for Tokenizer<S> {
    type Item = TokenizerOutput;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_token()
    }
}

/// Type yielded by [Tokenizer].
pub type TokenizerOutput = Result<TokenizerItem, TokenizerError>;

/// A token package yielded by [Tokenizer].
#[derive(Debug, PartialEq)]
pub struct TokenizerItem {
    pub token: Token,
    pub span: Range<SourceIndex>,
}

/// An error yielded by [Tokenizer].
#[derive(Debug, PartialEq, Clone)]
pub enum TokenizerError {
    UnexpectedTokenStart(SourceIndex, char),
    StringNotClosed(Range<SourceIndex>),
    StringInvalidEscape(Range<SourceIndex>, char),
    UnfinishedAndOperator(SourceIndex),
    UnfinishedOrOperator(SourceIndex),
    UnfinishedFloatLiteral(Range<SourceIndex>),
    IntLiteralOutOfRange(Range<SourceIndex>),
}

impl From<TokenizerError> for SourceError {
    fn from(error: TokenizerError) -> SourceError {
        match error {
            TokenizerError::UnexpectedTokenStart(span, char) => SourceError::new(
                span.span_one(),
                format!("TOKENIZER ERROR: Unexpected start of token: {}", char),
            ),
            TokenizerError::StringNotClosed(span) => SourceError::new(
                span,
                "TOKENIZER ERROR: String literal started but not finished".to_string(),
            ),
            TokenizerError::StringInvalidEscape(span, char) => SourceError::new(
                span,
                format!(
                    "TOKENIZER ERROR: Invalid escape sequence in string literal: \\{}",
                    char
                ),
            ),
            TokenizerError::UnfinishedAndOperator(span) => SourceError::new(
                span.span_one(),
                "TOKENIZER ERROR: Unexpected operator. Did you mean to use \"&&\"?".to_string(),
            ),
            TokenizerError::UnfinishedOrOperator(span) => SourceError::new(
                span.span_one(),
                "TOKENIZER ERROR: Unexpected operator. Did you mean to use \"||\"?".to_string(),
            ),
            TokenizerError::UnfinishedFloatLiteral(span) => SourceError::new(
                span,
                "TOKENIZER ERROR: Unfinished float literal. End with .0 instead".to_string(),
            ),
            TokenizerError::IntLiteralOutOfRange(span) => SourceError::new(
                span,
                format!(
                    "TOKENIZER ERROR: Int literal out of range. Supported range is <-{0}, {0}>",
                    i64::MAX
                ),
            ),
        }
    }
}

/// A buffer used internaly by the tokenizer to build a token across function calls.
struct TokenBuffer {
    pub text: String,
    pub start: SourceIndex,
}

impl TokenBuffer {
    pub fn new(char: Option<char>, start: SourceIndex) -> Self {
        Self {
            text: if let Some(c) = char {
                c.into()
            } else {
                String::new()
            },
            start,
        }
    }
}

#[cfg(test)]
mod test;
