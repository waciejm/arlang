//! Tokens returned by the tokenizer.

use std::fmt::Display;

pub mod prelude {
    pub use super::{Keyword::*, Literal::*, Operator::*, Symbol::*, Token, Type::*};
}

/// All tokens.
#[derive(Debug, PartialEq)]
pub enum Token {
    Keyword(Keyword),
    Type(Type),
    Operator(Operator),
    Symbol(Symbol),
    Literal(Literal),
    Identifier(String),
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Token::Keyword(keyword) => write!(f, "{}", keyword),
            Token::Type(r#type) => write!(f, "{}", r#type),
            Token::Operator(operator) => write!(f, "{}", operator),
            Token::Symbol(symbol) => write!(f, "{}", symbol),
            Token::Literal(literal) => write!(f, "{}", literal),
            Token::Identifier(identifier) => write!(f, "{}", identifier),
        }
    }
}

/// Tokens containing keywords.
#[derive(Debug, PartialEq)]
pub enum Keyword {
    /// `fn`
    Fn,
    /// `let`
    Let,
    /// `if`
    If,
    /// `elif`
    Elif,
    /// `else`
    Else,
    /// `while`
    While,
    /// `return`
    Return,
}

impl Display for Keyword {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Keyword::Fn => "keyword `fn`",
                Keyword::Let => "keyword `let`",
                Keyword::If => "keyword `if`",
                Keyword::Elif => "keyword `elif`",
                Keyword::Else => "keyword `else`",
                Keyword::While => "keyword `while`",
                Keyword::Return => "keyword `return`",
            }
        )
    }
}

/// Tokens containing primitive types.
#[derive(Debug, PartialEq)]
pub enum Type {
    /// `Unit`
    UnitT,
    /// `Int`
    IntT,
    /// `Float`
    FloatT,
    /// `Bool`
    BoolT,
    /// `String`
    StringT,
}

impl Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Type::UnitT => "type `Unit`",
                Type::IntT => "type `Int`",
                Type::FloatT => "type `Float`",
                Type::BoolT => "type `Bool`",
                Type::StringT => "type `String`",
            }
        )
    }
}

/// Tokens containing operators.
#[derive(Debug, PartialEq)]
pub enum Operator {
    /// `+`
    Plus,
    /// `-`
    Minus,
    /// `*`
    Multiply,
    /// `/`
    Divide,
    /// `<`
    Less,
    /// `<=`
    LessEqual,
    /// `>`
    More,
    /// `>=`
    MoreEqual,
    /// `=`
    Assignment,
    /// `==`
    Equal,
    /// `!=`
    NotEqual,
    /// `&&`
    And,
    /// `||`
    Or,
    /// `!`
    Not,
    /// `as`
    As,
}

impl Display for Operator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Operator::Plus => "operator `+`",
                Operator::Minus => "operator `-`",
                Operator::Multiply => "operator `*`",
                Operator::Divide => "operator `/`",
                Operator::Less => "operator `<`",
                Operator::LessEqual => "operator `<=`",
                Operator::More => "operator `>`",
                Operator::MoreEqual => "operator `>=`",
                Operator::Assignment => "operator `=`",
                Operator::Equal => "operator `==`",
                Operator::NotEqual => "operator `!=`",
                Operator::And => "operator `&&`",
                Operator::Or => "operator `||`",
                Operator::Not => "operator `!`",
                Operator::As => "operator `as`",
            }
        )
    }
}

/// Tokens containing literals.
#[derive(Debug, PartialEq)]
pub enum Literal {
    /// e.g. `1`
    IntL(i64),
    /// e.g. `1.0`
    FloatL(f64),
    /// `true` or `false`
    BoolL(bool),
    /// e.g. `"hello world"`
    StringL(String),
}

impl Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Literal::IntL(_) => "Int literal",
                Literal::FloatL(_) => "Float literal",
                Literal::BoolL(_) => "Bool literal",
                Literal::StringL(_) => "String literal",
            }
        )
    }
}

/// Tokens containing syntax symbols.
#[derive(Debug, PartialEq)]
pub enum Symbol {
    /// `,`
    Comma,
    /// `:`
    Colon,
    /// `;`
    Semicolon,
    /// `(`
    LParen,
    /// `)`
    RParen,
    /// `[`
    LBracket,
    /// `]`
    RBracket,
    /// `{`
    LBrace,
    /// `}`
    RBrace,
}

impl Display for Symbol {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Symbol::Comma => "symbol `,`",
                Symbol::Colon => "symbol `:`",
                Symbol::Semicolon => "symbol `;`",
                Symbol::LParen => "symbol `(`",
                Symbol::RParen => "symbol `)`",
                Symbol::LBracket => "symbol `[`",
                Symbol::RBracket => "symbol `]`",
                Symbol::LBrace => "symbol `{`",
                Symbol::RBrace => "symbol `}`",
            }
        )
    }
}
