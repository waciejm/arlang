use crate::source::{SourceIndex, SourceIter};

use super::{token::prelude::*, Tokenizer, TokenizerError, TokenizerItem};

#[test]
fn identifier_token() {
    test_single_token(
        " _ident_IFIER_0123 ",
        TokenizerItem {
            token: Token::Identifier("_ident_IFIER_0123".to_string()),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 18 },
        },
    );
}

#[test]
fn every_keyword_token() {
    test_single_token(
        " fn ",
        TokenizerItem {
            token: Token::Keyword(Fn),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " let ",
        TokenizerItem {
            token: Token::Keyword(Let),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 4 },
        },
    );
    test_single_token(
        " if ",
        TokenizerItem {
            token: Token::Keyword(If),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " elif ",
        TokenizerItem {
            token: Token::Keyword(Elif),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 5 },
        },
    );
    test_single_token(
        " else ",
        TokenizerItem {
            token: Token::Keyword(Else),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 5 },
        },
    );
    test_single_token(
        " while ",
        TokenizerItem {
            token: Token::Keyword(While),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 6 },
        },
    );
}

#[test]
fn every_type_token() {
    test_single_token(
        " Unit ",
        TokenizerItem {
            token: Token::Type(UnitT),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 5 },
        },
    );
    test_single_token(
        " Int ",
        TokenizerItem {
            token: Token::Type(IntT),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 4 },
        },
    );
    test_single_token(
        " Float ",
        TokenizerItem {
            token: Token::Type(FloatT),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 6 },
        },
    );
    test_single_token(
        " Bool ",
        TokenizerItem {
            token: Token::Type(BoolT),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 5 },
        },
    );
    test_single_token(
        " String ",
        TokenizerItem {
            token: Token::Type(StringT),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 7 },
        },
    );
}

#[test]
fn every_operator_token() {
    test_single_token(
        " + ",
        TokenizerItem {
            token: Token::Operator(Plus),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " - ",
        TokenizerItem {
            token: Token::Operator(Minus),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " * ",
        TokenizerItem {
            token: Token::Operator(Multiply),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " / ",
        TokenizerItem {
            token: Token::Operator(Divide),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " < ",
        TokenizerItem {
            token: Token::Operator(Less),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " <= ",
        TokenizerItem {
            token: Token::Operator(LessEqual),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " > ",
        TokenizerItem {
            token: Token::Operator(More),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " >= ",
        TokenizerItem {
            token: Token::Operator(MoreEqual),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " = ",
        TokenizerItem {
            token: Token::Operator(Assignment),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " == ",
        TokenizerItem {
            token: Token::Operator(Equal),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " != ",
        TokenizerItem {
            token: Token::Operator(NotEqual),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " && ",
        TokenizerItem {
            token: Token::Operator(And),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " || ",
        TokenizerItem {
            token: Token::Operator(Or),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_single_token(
        " ! ",
        TokenizerItem {
            token: Token::Operator(Not),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " as ",
        TokenizerItem {
            token: Token::Operator(As),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        },
    );
}

#[test]
fn every_literal_token() {
    test_single_token(
        " 123 ",
        TokenizerItem {
            token: Token::Literal(IntL(123)),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 4 },
        },
    );
    test_single_token(
        " 12.34 ",
        TokenizerItem {
            token: Token::Literal(FloatL(12.34)),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 6 },
        },
    );
    test_single_token(
        " true ",
        TokenizerItem {
            token: Token::Literal(BoolL(true)),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 5 },
        },
    );
    test_single_token(
        " false ",
        TokenizerItem {
            token: Token::Literal(BoolL(false)),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 6 },
        },
    );
    test_single_token(
        r#" "hello world" "#,
        TokenizerItem {
            token: Token::Literal(StringL("hello world".to_string())),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 14 },
        },
    );
}

#[test]
fn every_symbol_token() {
    test_single_token(
        " , ",
        TokenizerItem {
            token: Token::Symbol(Comma),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " : ",
        TokenizerItem {
            token: Token::Symbol(Colon),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " ; ",
        TokenizerItem {
            token: Token::Symbol(Semicolon),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " ( ",
        TokenizerItem {
            token: Token::Symbol(LParen),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " ) ",
        TokenizerItem {
            token: Token::Symbol(RParen),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " [ ",
        TokenizerItem {
            token: Token::Symbol(LBracket),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " ] ",
        TokenizerItem {
            token: Token::Symbol(RBracket),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " { ",
        TokenizerItem {
            token: Token::Symbol(LBrace),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
    test_single_token(
        " } ",
        TokenizerItem {
            token: Token::Symbol(RBrace),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
        },
    );
}

#[test]
fn string_literal_escapes() {
    test_single_token(
        r#" " \n \\ \" " "#,
        TokenizerItem {
            token: Token::Literal(StringL(" \n \\ \" ".to_string())),
            span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 13 },
        },
    );
}

#[test]
fn example_function_token_stream() {
    test_token_stream(
        "\
    fn example(a: Int, b: [String]): Unit {
        // example function
        let c: Unit = ();
        return ();
    }
",
        &[
            Token::Keyword(Fn),
            Token::Identifier("example".to_string()),
            Token::Symbol(LParen),
            Token::Identifier("a".to_string()),
            Token::Symbol(Colon),
            Token::Type(IntT),
            Token::Symbol(Comma),
            Token::Identifier("b".to_string()),
            Token::Symbol(Colon),
            Token::Symbol(LBracket),
            Token::Type(StringT),
            Token::Symbol(RBracket),
            Token::Symbol(RParen),
            Token::Symbol(Colon),
            Token::Type(UnitT),
            Token::Symbol(LBrace),
            Token::Keyword(Let),
            Token::Identifier("c".to_string()),
            Token::Symbol(Colon),
            Token::Type(UnitT),
            Token::Operator(Assignment),
            Token::Symbol(LParen),
            Token::Symbol(RParen),
            Token::Symbol(Semicolon),
            Token::Keyword(Return),
            Token::Symbol(LParen),
            Token::Symbol(RParen),
            Token::Symbol(Semicolon),
            Token::Symbol(RBrace),
        ],
    )
}

#[test]
fn function_no_parameters_regression_test() {
    test_token_stream(
        "\
    fn example() {}
",
        &[
            Token::Keyword(Fn),
            Token::Identifier("example".to_string()),
            Token::Symbol(LParen),
            Token::Symbol(RParen),
            Token::Symbol(LBrace),
            Token::Symbol(RBrace),
        ],
    )
}

#[test]
fn example_program_tokenizes() {
    let source_iter = SourceIter::new(include_bytes!("../../example.ar") as &[u8]);
    let tokenizer = Tokenizer::new(source_iter);
    tokenizer.for_each(|ti| assert!(ti.is_ok()));
}

#[test]
fn tokenizer_errors() {
    test_single_error(
        " ą ",
        TokenizerError::UnexpectedTokenStart(SourceIndex { line: 0, char: 1 }, 'ą'),
    );
    test_single_error(
        r#" " "#,
        TokenizerError::StringNotClosed(
            SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        ),
    );
    test_single_error(
        r#" "\a" "#,
        TokenizerError::StringInvalidEscape(
            SourceIndex { line: 0, char: 2 }..SourceIndex { line: 0, char: 4 },
            'a',
        ),
    );
    test_single_error(
        " & ",
        TokenizerError::UnfinishedAndOperator(SourceIndex { line: 0, char: 1 }),
    );
    test_single_error(
        " | ",
        TokenizerError::UnfinishedOrOperator(SourceIndex { line: 0, char: 1 }),
    );
    test_single_error(
        " 1. ",
        TokenizerError::UnfinishedFloatLiteral(
            SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
        ),
    );
    test_single_error(
        " 10000000000000000000000000000 ",
        TokenizerError::IntLiteralOutOfRange(
            SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 30 },
        ),
    );
}

#[test]
fn tricky_token_repetitions() {
    test_two_tokens(
        "===",
        TokenizerItem {
            token: Token::Operator(Equal),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 2 },
        },
        TokenizerItem {
            token: Token::Operator(Assignment),
            span: SourceIndex { line: 0, char: 2 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_two_tokens(
        "<==",
        TokenizerItem {
            token: Token::Operator(LessEqual),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 2 },
        },
        TokenizerItem {
            token: Token::Operator(Assignment),
            span: SourceIndex { line: 0, char: 2 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_two_tokens(
        ">==",
        TokenizerItem {
            token: Token::Operator(MoreEqual),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 2 },
        },
        TokenizerItem {
            token: Token::Operator(Assignment),
            span: SourceIndex { line: 0, char: 2 }..SourceIndex { line: 0, char: 3 },
        },
    );
    test_two_tokens(
        "!==",
        TokenizerItem {
            token: Token::Operator(NotEqual),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 2 },
        },
        TokenizerItem {
            token: Token::Operator(Assignment),
            span: SourceIndex { line: 0, char: 2 }..SourceIndex { line: 0, char: 3 },
        },
    );
}

fn test_single_token(src: &str, expected: TokenizerItem) {
    let source_iter = SourceIter::new(src.as_bytes());
    let tokenizer = Tokenizer::new(source_iter);
    let tokens = tokenizer.map(|m| m.unwrap()).collect::<Vec<_>>();
    assert!(tokens.len() == 1);
    assert_eq!(expected, tokens[0]);
}

fn test_two_tokens(src: &str, expected1: TokenizerItem, expected2: TokenizerItem) {
    let source_iter = SourceIter::new(src.as_bytes());
    let tokenizer = Tokenizer::new(source_iter);
    let tokens = tokenizer.map(|m| m.unwrap()).collect::<Vec<_>>();
    assert!(tokens.len() == 2);
    assert_eq!(expected1, tokens[0]);
    assert_eq!(expected2, tokens[1]);
}

fn test_token_stream(src: &str, expected_list: &[Token]) {
    let source_iter = SourceIter::new(src.as_bytes());
    let tokenizer = Tokenizer::new(source_iter);
    let tokens = tokenizer.map(|m| m.unwrap()).collect::<Vec<_>>();
    for index in 0..tokens.len().max(expected_list.len()) {
        assert_eq!(expected_list[index], tokens[index].token)
    }
}

fn test_single_error(src: &str, expected: TokenizerError) {
    let source_iter = SourceIter::new(src.as_bytes());
    let tokenizer = Tokenizer::new(source_iter);
    let tokens = tokenizer.map(|m| m.unwrap_err()).collect::<Vec<_>>();
    assert!(tokens.len() == 1);
    assert_eq!(expected, tokens[0]);
}
