//! Convinience checks for token parsing.

/// Trait to extend char to have token specific check methods.
pub trait TokenCharCheck {
    fn is_identifier_start(&self) -> bool;
    fn is_identifier_continuation(&self) -> bool;
    fn is_operator_start(&self) -> bool;
    fn is_syntax_symbol(&self) -> bool;
}

const IDENTIFIER_STARTS: &str = "_AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
const IDENTIFIER_CONTIMUATIONS: &str =
    "_AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
const OPERATOR_STARTS: &str = "+-=*/<>!&|";
const SYNTAX_SYMBOLS: &str = ",:;()[]{}";

impl TokenCharCheck for char {
    fn is_identifier_start(&self) -> bool {
        IDENTIFIER_STARTS.contains(*self)
    }

    fn is_identifier_continuation(&self) -> bool {
        IDENTIFIER_CONTIMUATIONS.contains(*self)
    }

    fn is_operator_start(&self) -> bool {
        OPERATOR_STARTS.contains(*self)
    }

    fn is_syntax_symbol(&self) -> bool {
        SYNTAX_SYMBOLS.contains(*self)
    }
}
