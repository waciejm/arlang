use std::{collections::HashMap, ops::Range};

use rayon::prelude::*;

use crate::{
    common::{scope::ScopeStack, Literal, Type},
    interpreter::ir,
    parser::syntax_tree::{
        operator::{BinaryOperator, UnaryOperator},
        ArrayInit, Expression, Function, Statement, SyntaxTree,
    },
    source::{SourceError, SourceIndex},
};

macro_rules! make_ok_expr_function_call {
    ($function:expr, $args:expr, $span:expr, $type:expr $(,)?) => {
        Ok((
            ir::Expression::FunctionCall {
                function: $function,
                arguments: $args,
                span: $span,
            },
            $type,
        ))
    };
}

/// Semantic analyzer.
/// Analyzes an [AST](crate::parser::syntax_tree::SyntaxTree)
/// and transforms it into [IRCode](crate::interpreter::ir::IRCode).
/// Can analyze functions in parallel.
pub struct SemanticAnalyzer {
    ast: SyntaxTree,
    functions: HashMap<String, DeclaredFunction>,
    parallel_analysis: bool,
}

impl SemanticAnalyzer {
    pub fn new(ast: SyntaxTree, parallel_analysis: bool) -> Self {
        Self {
            ast,
            functions: HashMap::new(),
            parallel_analysis,
        }
    }

    pub fn analyze(mut self) -> Result<ir::IRCode, AnalyzerError> {
        self.collect_functions()?;
        self.check_entry_point_present()?;
        let functions = self.analyze_functions()?;
        Ok(ir::IRCode::new(functions))
    }
}

impl SemanticAnalyzer {
    fn collect_functions(&mut self) -> Result<(), AnalyzerError> {
        for function in &self.ast.functions {
            if ir::BuiltInFunction::NAMES.contains(&function.identifier.as_str()) {
                return Err(AnalyzerError::BuiltinFnRedefinition(
                    function.identifier.clone(),
                    function.identifier_span.clone(),
                ));
            } else if self.functions.contains_key(&function.identifier) {
                return Err(AnalyzerError::FunctionRedefinition(
                    function.identifier.clone(),
                    function.identifier_span.clone(),
                ));
            } else {
                self.functions.insert(
                    function.identifier.clone(),
                    DeclaredFunction {
                        arguments: function.arguments.iter().map(|arg| arg.r#type).collect(),
                        return_type: function.return_type.unwrap_or(Type::Unit),
                    },
                );
            }
        }
        Ok(())
    }

    fn check_entry_point_present(&mut self) -> Result<(), AnalyzerError> {
        if self.functions.contains_key("main") {
            if !self.functions["main"].arguments.is_empty()
                || self.functions["main"].return_type != Type::Unit
            {
                let main = self
                    .ast
                    .functions
                    .iter()
                    .find(|f| f.identifier == "main")
                    .unwrap();
                Err(AnalyzerError::InvalidMainFnSignature(
                    main.signature_span.clone(),
                ))
            } else {
                Ok(())
            }
        } else {
            Err(AnalyzerError::NoMainFn)
        }
    }

    fn analyze_functions(
        &mut self,
    ) -> Result<HashMap<String, ir::UserDefinedFunction>, AnalyzerError> {
        let mut functions = HashMap::new();
        match self.parallel_analysis {
            false => {
                for function in self.ast.functions.iter() {
                    functions.insert(
                        function.identifier.clone(),
                        self.analyze_function(function)?,
                    );
                }
            }
            true => {
                let mut results = Vec::new();
                self.ast
                    .functions
                    .par_iter()
                    .map(|function| match self.analyze_function(function) {
                        Ok(analyzed_function) => {
                            Ok((function.identifier.clone(), analyzed_function))
                        }
                        Err(e) => Err(e),
                    })
                    .collect_into_vec(&mut results);

                for result in results {
                    let (identifier, function) = result?;
                    functions.insert(identifier, function);
                }
            }
        };
        Ok(functions)
    }

    fn analyze_function(
        &self,
        function: &Function,
    ) -> Result<ir::UserDefinedFunction, AnalyzerError> {
        let mut scope = ScopeStack::<Type>::new();
        scope.open_function();

        let mut arguments = Vec::new();
        for argument in function.arguments.iter() {
            arguments.push(argument.identifier.clone());
            scope.declare_variable(argument.identifier.clone(), argument.r#type);
        }

        let mut body = Vec::new();
        for statement in function.body.iter() {
            body.push(self.analyze_statement(
                function.return_type.unwrap_or(Type::Unit),
                &mut scope,
                statement,
            )?);
        }

        self.validate_last_statement_in_function(function)?;

        Ok(ir::UserDefinedFunction { arguments, body })
    }

    fn validate_last_statement_in_function(
        &self,
        function: &Function,
    ) -> Result<(), AnalyzerError> {
        match function.return_type {
            Some(Type::Unit) => Ok(()),
            Some(t) => match function.body.last() {
                Some(Statement::Return { .. }) => Ok(()),
                Some(Statement::Expression(Expression::Call { identifier, .. }))
                    if identifier == "panic" =>
                {
                    Ok(())
                }
                Some(_) | None => Err(AnalyzerError::InvalidLastFnStatement {
                    expected_type: t,
                    span: function.span.clone(),
                }),
            },
            None => Ok(()),
        }
    }

    fn analyze_statement(
        &self,
        function_type: Type,
        scope: &mut ScopeStack<Type>,
        statement: &Statement,
    ) -> Result<ir::Statement, AnalyzerError> {
        match statement {
            Statement::Declaration { .. } => self.analyze_declaration_statement(scope, statement),
            Statement::Return { .. } => {
                self.analyze_return_statement(function_type, scope, statement)
            }
            Statement::If { .. } => self.analyze_if_statement(function_type, scope, statement),
            Statement::While { .. } => {
                self.analyze_while_statement(function_type, scope, statement)
            }
            Statement::Expression(expression) => Ok(ir::Statement::Expression(
                self.analyze_expression(scope, expression)?.0,
            )),
        }
    }

    fn analyze_declaration_statement(
        &self,
        scope: &mut ScopeStack<Type>,
        statement: &Statement,
    ) -> Result<ir::Statement, AnalyzerError> {
        if let Statement::Declaration {
            identifier,
            r#type,
            value,
            span,
        } = statement
        {
            let (expr, expr_type) = self.analyze_expression(scope, value)?;
            if r#type.unwrap_or(expr_type) != expr_type {
                Err(AnalyzerError::DeclarationTypeConflict {
                    declared_type: r#type.unwrap(),
                    value_type: expr_type,
                    span: span.clone(),
                })
            } else {
                scope.declare_variable(identifier.clone(), expr_type);
                Ok(ir::Statement::Declaration {
                    identifier: identifier.clone(),
                    value: expr,
                })
            }
        } else {
            panic!()
        }
    }

    fn analyze_return_statement(
        &self,
        function_type: Type,
        scope: &ScopeStack<Type>,
        statement: &Statement,
    ) -> Result<ir::Statement, AnalyzerError> {
        if let Statement::Return { value, span } = statement {
            let (expr, expr_type) = self.analyze_expression(scope, value)?;
            if expr_type == function_type {
                Ok(ir::Statement::Return(expr))
            } else {
                Err(AnalyzerError::InvalidReturnType {
                    function_type,
                    return_type: expr_type,
                    span: span.clone(),
                })
            }
        } else {
            panic!()
        }
    }

    fn analyze_if_statement(
        &self,
        function_type: Type,
        scope: &mut ScopeStack<Type>,
        statement: &Statement,
    ) -> Result<ir::Statement, AnalyzerError> {
        if let Statement::If {
            branches,
            else_branch,
        } = statement
        {
            let mut ir_branches = Vec::new();
            for branch in branches {
                let (condition_expr, condition_type) = self.analyze_expression(scope, &branch.0)?;
                if condition_type != Type::Bool {
                    return Err(AnalyzerError::InvalidConditionType(
                        condition_type,
                        branch.0.span().clone(),
                    ));
                } else {
                    scope.open_block();
                    let mut ir_body = Vec::new();
                    for statement in &branch.1 {
                        ir_body.push(self.analyze_statement(function_type, scope, statement)?);
                    }
                    ir_branches.push((condition_expr, ir_body));
                    scope.close_block();
                }
            }
            let mut ir_else_branch = Vec::new();
            if let Some(else_branch) = else_branch {
                scope.open_block();
                for statement in else_branch {
                    ir_else_branch.push(self.analyze_statement(function_type, scope, statement)?);
                }
                scope.close_block();
            }
            Ok(ir::Statement::If {
                branches: ir_branches,
                else_branch: ir_else_branch,
            })
        } else {
            panic!()
        }
    }

    fn analyze_while_statement(
        &self,
        function_type: Type,
        scope: &mut ScopeStack<Type>,
        statement: &Statement,
    ) -> Result<ir::Statement, AnalyzerError> {
        if let Statement::While { condition, body } = statement {
            let (condition_expr, condition_type) = self.analyze_expression(scope, condition)?;
            if condition_type != Type::Bool {
                Err(AnalyzerError::InvalidConditionType(
                    condition_type,
                    condition.span().clone(),
                ))
            } else {
                scope.open_block();
                let mut ir_body = Vec::new();
                for statement in body {
                    ir_body.push(self.analyze_statement(function_type, scope, statement)?);
                }
                scope.close_block();
                Ok(ir::Statement::While {
                    condition: condition_expr,
                    body: ir_body,
                })
            }
        } else {
            panic!()
        }
    }

    fn analyze_expression(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        match expression {
            Expression::UnaryOp { .. } => self.analyze_expression_unary_op(scope, expression),
            Expression::BinaryOp {
                operator,
                left,
                right,
                span,
            } => match operator {
                BinaryOperator::Assignment => {
                    self.analyze_expression_binary_op_assignment(scope, left, right, span)
                }
                _ => self.analyze_expression_binary_op_other(scope, operator, left, right, span),
            },
            Expression::Cast { .. } => self.analyze_expression_cast(scope, expression),
            Expression::Identifier { .. } => self.analyze_expression_identifier(scope, expression),
            Expression::ArrayIndex { .. } => self.analyze_expression_array_index(scope, expression),
            Expression::Call { .. } => self.analyze_expression_call(scope, expression),
            Expression::Literal { literal, .. } => Ok((
                ir::Expression::Value(literal.clone()),
                literal.type_of_self(),
            )),
            Expression::ArrayInit { .. } => self.analyze_expression_array_init(scope, expression),
        }
    }

    fn analyze_expression_unary_op(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        if let Expression::UnaryOp {
            operator,
            expression,
            span,
        } = expression
        {
            let (expression, expression_type) = self.analyze_expression(scope, expression)?;
            if operator.is_type_accepted(&expression_type) {
                make_ok_expr_function_call!(
                    ir::CallableFunction::Operator(ir::OperatorFunction::from(*operator)),
                    vec![expression],
                    span.clone(),
                    operator.result_type(&expression_type),
                )
            } else {
                Err(AnalyzerError::InvalidUnaryOpType {
                    operator: *operator,
                    expr_type: expression_type,
                    span: span.clone(),
                })
            }
        } else {
            panic!()
        }
    }

    fn analyze_expression_binary_op_assignment(
        &self,
        scope: &ScopeStack<Type>,
        left: &Expression,
        right: &Expression,
        span: &Range<SourceIndex>,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        let (_, left_type) = self.analyze_expression(scope, left)?;
        let (ir_right, right_type) = self.analyze_expression(scope, right)?;
        match left {
            Expression::Identifier { identifier, .. } => {
                if BinaryOperator::Assignment.are_types_accepted(&left_type, &right_type) {
                    make_ok_expr_function_call!(
                        ir::CallableFunction::Assignment(ir::AssignmentFunction::Variable),
                        vec![
                            ir::Expression::Value(Literal::String(identifier.clone())),
                            ir_right,
                        ],
                        span.clone(),
                        BinaryOperator::Assignment.result_type(&left_type, &right_type),
                    )
                } else {
                    Err(AnalyzerError::InvalidRightSideOfAssignment {
                        expected: left_type,
                        found: right_type,
                        span: right.span().clone(),
                    })
                }
            }
            Expression::ArrayIndex { array, index, .. } => {
                if BinaryOperator::Assignment.are_types_accepted(&left_type, &right_type) {
                    make_ok_expr_function_call!(
                        ir::CallableFunction::Assignment(ir::AssignmentFunction::Array),
                        vec![
                            self.analyze_expression(scope, array)?.0,
                            self.analyze_expression(scope, index)?.0,
                            ir_right,
                        ],
                        span.clone(),
                        BinaryOperator::Assignment.result_type(&left_type, &right_type),
                    )
                } else {
                    Err(AnalyzerError::InvalidRightSideOfAssignment {
                        expected: left_type,
                        found: right_type,
                        span: right.span().clone(),
                    })
                }
            }
            _ => Err(AnalyzerError::InvalidLeftSideOfAssignment(
                left.span().clone(),
            )),
        }
    }

    fn analyze_expression_binary_op_other(
        &self,
        scope: &ScopeStack<Type>,
        operator: &BinaryOperator,
        left: &Expression,
        right: &Expression,
        span: &Range<SourceIndex>,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        let (ir_left, left_type) = self.analyze_expression(scope, left)?;
        let (ir_right, right_type) = self.analyze_expression(scope, right)?;
        if operator.are_types_accepted(&left_type, &right_type) {
            make_ok_expr_function_call!(
                ir::CallableFunction::Operator(ir::OperatorFunction::from(*operator)),
                vec![ir_left, ir_right],
                span.clone(),
                operator.result_type(&left_type, &right_type),
            )
        } else {
            Err(AnalyzerError::InvalidBinaryOpTypes {
                operator: *operator,
                left_type,
                right_type,
                span: span.clone(),
            })
        }
    }

    fn analyze_expression_cast(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        if let Expression::Cast {
            expression,
            cast_type,
            span,
        } = expression
        {
            let (expression, expression_type) = self.analyze_expression(scope, expression)?;
            match (expression_type, cast_type) {
                (Type::Int, Type::Float) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::IntToFloat),
                    vec![expression],
                    span.clone(),
                    Type::Float,
                ),
                (Type::Int, Type::String) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::IntToString),
                    vec![expression],
                    span.clone(),
                    Type::String,
                ),
                (Type::Float, Type::Int) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::FloatToInt),
                    vec![expression],
                    span.clone(),
                    Type::Int,
                ),
                (Type::Float, Type::String) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::FloatToString),
                    vec![expression],
                    span.clone(),
                    Type::String,
                ),
                (Type::Bool, Type::Int) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::BoolToInt),
                    vec![expression],
                    span.clone(),
                    Type::Int,
                ),
                (Type::Bool, Type::Float) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::BoolToFloat),
                    vec![expression],
                    span.clone(),
                    Type::Float,
                ),
                (Type::Bool, Type::String) => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::BoolToString),
                    vec![expression],
                    span.clone(),
                    Type::String,
                ),
                (a, &b) if a == b => make_ok_expr_function_call!(
                    ir::CallableFunction::Cast(ir::CastFunction::Identity),
                    vec![expression],
                    span.clone(),
                    a,
                ),
                _ => Err(AnalyzerError::InvalidCast {
                    from: expression_type,
                    to: *cast_type,
                    span: span.clone(),
                }),
            }
        } else {
            panic!()
        }
    }

    fn analyze_expression_identifier(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        if let Expression::Identifier { identifier, span } = expression {
            if scope.has_variable(identifier) {
                Ok((
                    ir::Expression::Variable(identifier.clone()),
                    *scope.get_variable(identifier),
                ))
            } else {
                Err(AnalyzerError::VariableNotInScope(
                    identifier.clone(),
                    span.clone(),
                ))
            }
        } else {
            panic!()
        }
    }

    fn analyze_expression_array_index(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        if let Expression::ArrayIndex { array, index, span } = expression {
            let (ir_array, array_type) = self.analyze_expression(scope, array.as_ref())?;
            if array_type.array_inner().is_none() {
                return Err(AnalyzerError::InvalidArrayTypeInArrayIndex(
                    array_type,
                    array.span().clone(),
                ));
            }
            let (ir_index, index_type) = self.analyze_expression(scope, index)?;
            if index_type != Type::Int {
                return Err(AnalyzerError::InvalidIndexTypeInArrayIndex(
                    index_type,
                    index.span().clone(),
                ));
            }
            make_ok_expr_function_call!(
                ir::CallableFunction::ArrayIndex(ir::ArrayIndexFunction {}),
                vec![ir_array, ir_index],
                span.clone(),
                array_type.array_inner().unwrap(),
            )
        } else {
            panic!()
        }
    }

    fn analyze_expression_call(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        if let Expression::Call {
            identifier,
            arguments,
            span,
        } = expression
        {
            let mut analyzed_values = Vec::new();
            let mut analyzed_types = Vec::new();
            for argument in arguments {
                let (analyzed_value, analyzed_type) = self.analyze_expression(scope, argument)?;
                analyzed_values.push(analyzed_value);
                analyzed_types.push(analyzed_type);
            }
            if self.functions.contains_key(identifier) {
                let declared_function = &self.functions[identifier];
                if analyzed_types == declared_function.arguments {
                    make_ok_expr_function_call!(
                        ir::CallableFunction::UserDefined(identifier.clone()),
                        analyzed_values,
                        span.clone(),
                        declared_function.return_type,
                    )
                } else {
                    Err(AnalyzerError::InvalidFunctionCallArguments {
                        expected: Type::slice_to_string(&declared_function.arguments),
                        found: analyzed_types,
                        span: span.clone(),
                    })
                }
            } else if ir::BuiltInFunction::NAMES.contains(&identifier.as_str()) {
                let builtin_fn = ir::BuiltInFunction::get_by_identifier(identifier);
                if builtin_fn.are_expected_arguments(&analyzed_types) {
                    make_ok_expr_function_call!(
                        ir::CallableFunction::BuiltIn(builtin_fn),
                        analyzed_values,
                        span.clone(),
                        builtin_fn.return_type(),
                    )
                } else {
                    Err(AnalyzerError::InvalidFunctionCallArguments {
                        expected: builtin_fn.expected_arguments(),
                        found: analyzed_types,
                        span: span.clone(),
                    })
                }
            } else {
                Err(AnalyzerError::CallToUndefinedFn(
                    identifier.clone(),
                    span.clone(),
                ))
            }
        } else {
            panic!()
        }
    }

    fn analyze_expression_array_init(
        &self,
        scope: &ScopeStack<Type>,
        expression: &Expression,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        if let Expression::ArrayInit { init, span } = expression {
            match init.as_ref() {
                ArrayInit::List(expressions) => {
                    self.analyze_expression_array_init_list(scope, expressions, span)
                }
                ArrayInit::Repeat { value, repeat } => {
                    self.analyze_expression_array_init_repeat(scope, value, repeat, span)
                }
            }
        } else {
            panic!()
        }
    }

    fn analyze_expression_array_init_list(
        &self,
        scope: &ScopeStack<Type>,
        expressions: &[Expression],
        span: &Range<SourceIndex>,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        match expressions {
            [] => Err(AnalyzerError::ArrayInitListEmpty(span.clone())),
            [first, rest @ ..] => {
                let mut arguments = Vec::new();
                let (ir_first, first_type) = self.analyze_expression(scope, first)?;
                if first_type.array_of().is_none() {
                    return Err(AnalyzerError::ArrayTypeInArrayInit(first.span().clone()));
                }
                arguments.push(ir_first);
                for another in rest {
                    let (ir_another, another_type) = self.analyze_expression(scope, another)?;
                    if another_type.array_of().is_none() {
                        return Err(AnalyzerError::ArrayTypeInArrayInit(another.span().clone()));
                    } else if another_type != first_type {
                        return Err(AnalyzerError::ArrayInitListConflictingTypes {
                            first: first_type,
                            other: another_type,
                            span: another.span().clone(),
                        });
                    }
                    arguments.push(ir_another);
                }
                make_ok_expr_function_call!(
                    ir::CallableFunction::ArrayInit(ir::ArrayInitFunction::List),
                    arguments,
                    span.clone(),
                    first_type.array_of().unwrap(),
                )
            }
        }
    }

    fn analyze_expression_array_init_repeat(
        &self,
        scope: &ScopeStack<Type>,
        value: &Expression,
        repeat: &Expression,
        span: &Range<SourceIndex>,
    ) -> Result<(ir::Expression, Type), AnalyzerError> {
        let (ir_value, value_type) = self.analyze_expression(scope, value)?;
        if value_type.array_of().is_none() {
            return Err(AnalyzerError::ArrayTypeInArrayInit(value.span().clone()));
        }
        let (ir_repeat, repeat_type) = self.analyze_expression(scope, repeat)?;
        if repeat_type != Type::Int {
            return Err(AnalyzerError::ArrayInitRepeatNotInt(
                repeat_type,
                repeat.span().clone(),
            ));
        }
        make_ok_expr_function_call!(
            ir::CallableFunction::ArrayInit(ir::ArrayInitFunction::Repeat),
            vec![ir_value, ir_repeat],
            span.clone(),
            value_type.array_of().unwrap()
        )
    }
}

#[derive(Debug, PartialEq)]
pub enum AnalyzerError {
    BuiltinFnRedefinition(String, Range<SourceIndex>),
    FunctionRedefinition(String, Range<SourceIndex>),
    NoMainFn,
    InvalidMainFnSignature(Range<SourceIndex>),
    DeclarationTypeConflict {
        declared_type: Type,
        value_type: Type,
        span: Range<SourceIndex>,
    },
    InvalidReturnType {
        function_type: Type,
        return_type: Type,
        span: Range<SourceIndex>,
    },
    InvalidLastFnStatement {
        expected_type: Type,
        span: Range<SourceIndex>,
    },
    InvalidConditionType(Type, Range<SourceIndex>),
    InvalidUnaryOpType {
        operator: UnaryOperator,
        expr_type: Type,
        span: Range<SourceIndex>,
    },
    InvalidBinaryOpTypes {
        operator: BinaryOperator,
        left_type: Type,
        right_type: Type,
        span: Range<SourceIndex>,
    },
    InvalidLeftSideOfAssignment(Range<SourceIndex>),
    InvalidRightSideOfAssignment {
        expected: Type,
        found: Type,
        span: Range<SourceIndex>,
    },
    InvalidCast {
        from: Type,
        to: Type,
        span: Range<SourceIndex>,
    },
    VariableNotInScope(String, Range<SourceIndex>),
    InvalidArrayTypeInArrayIndex(Type, Range<SourceIndex>),
    InvalidIndexTypeInArrayIndex(Type, Range<SourceIndex>),
    CallToUndefinedFn(String, Range<SourceIndex>),
    InvalidFunctionCallArguments {
        expected: String,
        found: Vec<Type>,
        span: Range<SourceIndex>,
    },
    ArrayTypeInArrayInit(Range<SourceIndex>),
    ArrayInitRepeatNotInt(Type, Range<SourceIndex>),
    ArrayInitListEmpty(Range<SourceIndex>),
    ArrayInitListConflictingTypes {
        first: Type,
        other: Type,
        span: Range<SourceIndex>,
    },
}

impl From<AnalyzerError> for SourceError {
    fn from(error: AnalyzerError) -> Self {
        match error {
            AnalyzerError::BuiltinFnRedefinition(identifier, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Redefinition of a builtin function `{}`.", identifier),
                )
            }
            AnalyzerError::FunctionRedefinition(identifier, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Multiple definition of a function `{}`.", identifier),
                )
            }
            AnalyzerError::NoMainFn => {
                SourceError::new(
                    SourceIndex::span_empty(),
                    "ANALYZER ERROR: No `main` function definition.".to_string(),
                )
            }
            AnalyzerError::InvalidMainFnSignature(span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Invalid main function signature. Should be `main()` or `main(): {}`.", Type::Unit),
                )
            }
            AnalyzerError::DeclarationTypeConflict { declared_type, value_type, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Delcaration type conflict. Declared type is {}, type of value is {}.",
                        declared_type,
                        value_type,
                    ),
                )
            },
            AnalyzerError::InvalidReturnType { function_type, return_type, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Return type does not match function return type. Expected {}, found {}.",
                        function_type,
                        return_type,
                    ),
                )
            },
            AnalyzerError::InvalidLastFnStatement { expected_type, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Invalid last statement of function returning value. Expected return {} or panic().",
                        expected_type,
                    )
                )
            },
            AnalyzerError::InvalidConditionType(condition_type, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Condition value type is invalid. Expected {}, found {}.", Type::Bool, condition_type),
                )
            },
            AnalyzerError::InvalidUnaryOpType { operator, expr_type, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Unary operator {} applied to invalid type. Expected {}, found {}.",
                        operator,
                        operator.accepted_types(),
                        expr_type,
                    ),
                )
            },
            AnalyzerError::InvalidBinaryOpTypes { operator, left_type, right_type, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Binary operator {} applied to invalid type. Expected {}, found {} {} {}.",
                        operator,
                        operator.accepted_types(),
                        left_type,
                        operator,
                        right_type,
                    ),
                )
            }
            AnalyzerError::InvalidLeftSideOfAssignment(span) => {
                SourceError::new(
                    span,
                    "ANALYZER ERROR: Invalid left side of assignment. Expected variable or array indexing".to_string(),
                )
            },
            AnalyzerError::InvalidRightSideOfAssignment { expected, found, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Invalid right side of assignment. Expected {}, found {}.",
                        expected,
                        found,
                    ),
                )
            }
            AnalyzerError::InvalidCast { from, to, span } => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Invalid cast from {} to {}. Valid casts are {}.", from, to, ir::CastFunction::valid_casts()),
                )
            },
            AnalyzerError::VariableNotInScope(identifier, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: No variable named {} found in current scope.", identifier),
                )
            },
            AnalyzerError::InvalidArrayTypeInArrayIndex(array_type, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Array indexing on invalid type. Found {}, expected an array.", array_type),
                )
            },
            AnalyzerError::InvalidIndexTypeInArrayIndex(index_type, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Invalid index type in array indexing. Found {}, expected {}.", index_type, Type::Int),
                )
            },
            AnalyzerError::CallToUndefinedFn(identifier, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: No function named {} found.", identifier),
                )
            },
            AnalyzerError::InvalidFunctionCallArguments { expected, found, span } => {
                SourceError::new(
                    span,
                    format!(
                        "ANALYZER ERROR: Invalid function call arguments. Expected ({}), found ({})",
                        expected,
                        Type::slice_to_string(&found),
                    ),
                )
            },
            AnalyzerError::ArrayTypeInArrayInit(span) => {
                SourceError::new(
                    span,
                    "ANALYZER ERROR: Array types are not allowed inside arrays.".to_string(),
                )
            },
            AnalyzerError::ArrayInitRepeatNotInt(repeat_type, span) => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Invalid type of count in array init. Expected {}, found {}.", Type::Int, repeat_type),
                )
            },
            AnalyzerError::ArrayInitListEmpty(span) => {
                SourceError::new(
                    span,
                    "ANALYZER ERROR: Empty array initializations are not allowed. Use [T; 0] instead.".to_string(),
                )
            },
            AnalyzerError::ArrayInitListConflictingTypes { first, other, span } => {
                SourceError::new(
                    span,
                    format!("ANALYZER ERROR: Conflicting types in array init list. First element is {}, another is {}.", first, other),
                )
            },
        }
    }
}

#[derive(Debug)]
struct DeclaredFunction {
    pub arguments: Vec<Type>,
    pub return_type: Type,
}

#[cfg(test)]
mod test;
