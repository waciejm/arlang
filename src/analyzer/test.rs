use crate::{
    analyzer::AnalyzerError,
    common::{Literal, Type},
    interpreter::ir::{
        ArrayIndexFunction, ArrayInitFunction, AssignmentFunction, BuiltInFunction,
        CallableFunction, CastFunction, Expression, IRCode, OperatorFunction, Statement,
        UserDefinedFunction,
    },
    parser::syntax_tree::operator::{BinaryOperator, UnaryOperator},
    source::SourceIndex,
};

fn analyze_src(src: &str) -> Result<IRCode, AnalyzerError> {
    let code = src;
    let source_iter = crate::source::SourceIter::new(code.as_bytes());
    let tokenizer = crate::tokenizer::Tokenizer::new(source_iter);
    let parser = crate::parser::Parser::new(tokenizer);
    let ast = parser.parse().unwrap();
    let analyzer = crate::analyzer::SemanticAnalyzer::new(ast, false);
    analyzer.analyze()
}

fn src_ok(src: &str) {
    assert!(analyze_src(src).is_ok());
}

fn src_err(src: &str, error: AnalyzerError) {
    let result = analyze_src(src);
    assert!(result.is_err());
    assert_eq!(result.unwrap_err(), error);
}

#[test]
fn fn_definition() {
    src_ok(
        "\
fn main() {}
fn a() {}
fn b() {}
",
    );
    src_err(
        "fn print() {}",
        AnalyzerError::BuiltinFnRedefinition(
            "print".to_string(),
            SourceIndex::span((0, 3), (0, 8)),
        ),
    );
    src_err(
        "\
fn main() {}
fn a() {}
fn a() {}
",
        AnalyzerError::FunctionRedefinition("a".to_string(), SourceIndex::span((2, 3), (2, 4))),
    );
}

#[test]
fn main_fn() {
    src_ok("fn main() {}");
    src_err("fn a() {}", AnalyzerError::NoMainFn);
}

#[test]
fn main_fn_signature() {
    src_ok("fn main(): Unit {}");
    src_err(
        "fn main(a: Int) {}",
        AnalyzerError::InvalidMainFnSignature(SourceIndex::span((0, 3), (0, 15))),
    );
    src_err(
        "fn main(): Int {}",
        AnalyzerError::InvalidMainFnSignature(SourceIndex::span((0, 3), (0, 14))),
    );
}

#[test]
fn declaration_type_conflict() {
    src_ok(
        "\
fn main() {
    let a: Int = 1;
}
",
    );
    src_err(
        "\
fn main() {
    let a: Int = 1.0;
}
",
        AnalyzerError::DeclarationTypeConflict {
            declared_type: Type::Int,
            value_type: Type::Float,
            span: SourceIndex::span((1, 4), (1, 21)),
        },
    );
}

#[test]
fn return_type() {
    src_ok(
        "\
fn main() {
    return ();
}
",
    );
    src_err(
        "\
fn main() {
    return 1;
}
",
        AnalyzerError::InvalidReturnType {
            function_type: Type::Unit,
            return_type: Type::Int,
            span: SourceIndex::span((1, 4), (1, 13)),
        },
    );
}

#[test]
fn condition_type() {
    src_ok(
        "\
fn main() {
    if true {}
    elif true {}
    while true {}
}
",
    );
    src_err(
        "\
fn main() {
    if 1 {}
}
",
        AnalyzerError::InvalidConditionType(Type::Int, SourceIndex::span((1, 7), (1, 8))),
    );
    src_err(
        "\
fn main() {
    if true {}
    elif 2.0 {}
}
",
        AnalyzerError::InvalidConditionType(Type::Float, SourceIndex::span((2, 9), (2, 12))),
    );
    src_err(
        "\
fn main() {
    while \"true\" {}
}
",
        AnalyzerError::InvalidConditionType(Type::String, SourceIndex::span((1, 10), (1, 16))),
    );
}

#[test]
fn unary_op_type() {
    src_ok(
        "\
fn main() {
    !false;
    -1;
    -1.0;
}
",
    );
    src_err(
        "\
fn main() {
    !1;
}
",
        AnalyzerError::InvalidUnaryOpType {
            operator: UnaryOperator::Not,
            expr_type: Type::Int,
            span: SourceIndex::span((1, 4), (1, 6)),
        },
    );
    src_err(
        "\
fn main() {
    !1.0;
}
",
        AnalyzerError::InvalidUnaryOpType {
            operator: UnaryOperator::Not,
            expr_type: Type::Float,
            span: SourceIndex::span((1, 4), (1, 8)),
        },
    );
    src_err(
        "\
fn main() {
    -true;
}
",
        AnalyzerError::InvalidUnaryOpType {
            operator: UnaryOperator::Minus,
            expr_type: Type::Bool,
            span: SourceIndex::span((1, 4), (1, 9)),
        },
    );
}

#[test]
fn binary_op_types() {
    src_ok(
        "\
fn main() {
    1 + 1;
    1.0 + 1.0;
    \"a\" + \"b\";
    [(), ()] + [(), ()];
    [true, true] + [false, false];
    [1, 2] + [3, 4];
    [1.0, 2.0] + [3.0, 4.0];
    [\"a\", \"b\"] + [\"c\", \"d\"];

    1 - 1;
    1.0 - 1.0;

    1 * 1;
    1.0 * 1.0;

    1 / 1;
    1.0 / 1.0;

    1 < 1;
    1.0 < 1.0;

    1 <= 1;
    1.0 <= 1.0;

    1 > 1;
    1.0 > 1.0;

    1 >= 1;
    1.0 >= 1.0;

    1 == 1;
    1.0 == 1.0;
    \"abc\" == \"abc\";
    true == true;
    [(), ()] == [(), ()];
    [true, true] == [false, false];
    [1, 2] == [3, 4];
    [1.0, 2.0] == [3.0, 4.0];
    [\"a\", \"b\"] == [\"c\", \"d\"];

    1 != 1;
    1.0 != 1.0;
    \"abc\" != \"abc\";
    true != true;
    [(), ()] != [(), ()];
    [true, true] != [false, false];
    [1, 2] != [3, 4];
    [1.0, 2.0] != [3.0, 4.0];
    [\"a\", \"b\"] != [\"c\", \"d\"];

    true && true;

    true || true;
}
",
    );
    src_err(
        "fn main() { 1 + 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Plus,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 19)),
        },
    );
    src_err(
        "fn main() { 1 - 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Minus,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 19)),
        },
    );
    src_err(
        "fn main() { 1 * 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Multiply,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 19)),
        },
    );
    src_err(
        "fn main() { 1 / 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Divide,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 19)),
        },
    );
    src_err(
        "fn main() { 1 < 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Less,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 19)),
        },
    );
    src_err(
        "fn main() { 1 <= 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::LessEqual,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 20)),
        },
    );
    src_err(
        "fn main() { 1 > 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::More,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 19)),
        },
    );
    src_err(
        "fn main() { 1 >= 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::MoreEqual,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 20)),
        },
    );
    src_err(
        "fn main() { 1 == 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Equal,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 20)),
        },
    );
    src_err(
        "fn main() { 1 != 1.0; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::NotEqual,
            left_type: Type::Int,
            right_type: Type::Float,
            span: SourceIndex::span((0, 12), (0, 20)),
        },
    );
    src_err(
        "fn main() { 1 && 2; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::And,
            left_type: Type::Int,
            right_type: Type::Int,
            span: SourceIndex::span((0, 12), (0, 18)),
        },
    );
    src_err(
        "fn main() { 1 || 2; }",
        AnalyzerError::InvalidBinaryOpTypes {
            operator: BinaryOperator::Or,
            left_type: Type::Int,
            right_type: Type::Int,
            span: SourceIndex::span((0, 12), (0, 18)),
        },
    );
}

#[test]
fn assignment() {
    src_ok(
        "\
fn main () {
    let a = 1;
    a = 2;
    let b = [1, 0, 1];
    b[1] = 1;
    [0, 1, 1][0] = 1;
}
",
    );
    src_err(
        "fn main() { 1 = 1; }",
        AnalyzerError::InvalidLeftSideOfAssignment(SourceIndex::span((0, 12), (0, 13))),
    );
    src_err(
        "\
fn main() {
    a = 1;
}
",
        AnalyzerError::VariableNotInScope("a".to_string(), SourceIndex::span((1, 4), (1, 5))),
    );
    src_err(
        "\
fn main() {
    let a = 1;
    a = 1.0;
}
",
        AnalyzerError::InvalidRightSideOfAssignment {
            expected: Type::Int,
            found: Type::Float,
            span: SourceIndex::span((2, 8), (2, 11)),
        },
    );
}

#[test]
fn cast() {
    src_ok(
        "\
fn main() {
    1 as Float;
    1 as String;
    1.0 as Int;
    1.0 as String;
    true as Int;
    true as Float;
    true as String;
    () as Unit;
    1 as Int;
    1.0 as Float;
    true as Bool;
    \"asd\" as String;
    [()] as [Unit];
    [1] as [Int];
    [1.0] as [Float];
    [true] as [Bool];
    [\"asd\"] as [String];
}
",
    );
    src_err(
        "fn main() { 1 as Bool; }",
        AnalyzerError::InvalidCast {
            from: Type::Int,
            to: Type::Bool,
            span: SourceIndex::span((0, 12), (0, 21)),
        },
    )
}

#[test]
fn variable_scope() {
    src_ok(
        "\
fn main() {
    let a = 0;
    a = 1;
    if true {
        a = 2;
    }
}
",
    );
    src_err(
        "\
fn main() {
    let a = 0;
    if true {
        a = 1;
        let b = 1;
        b = 2;
    }
    b = 0;
}
",
        AnalyzerError::VariableNotInScope("b".to_string(), SourceIndex::span((7, 4), (7, 5))),
    )
}

#[test]
fn array_index() {
    src_ok("fn main() { [1, 2, 3][0]; }");
    src_err(
        "fn main() { 1[0]; }",
        AnalyzerError::InvalidArrayTypeInArrayIndex(Type::Int, SourceIndex::span((0, 12), (0, 13))),
    );
    src_err(
        "fn main() { [1, 2, 3][1.0]; }",
        AnalyzerError::InvalidIndexTypeInArrayIndex(
            Type::Float,
            SourceIndex::span((0, 22), (0, 25)),
        ),
    );
}

#[test]
fn function_call() {
    src_ok(
        "\
fn main () {
    let a: Int = a((), 1, 2.0, true, \"asdf\", [1, 2, 3]);
    print(\"asdf\");
}

fn a(a: Unit, b: Int, c: Float, d: Bool, e: String, f: [Int]): Int {
    return b;
}
",
    );
    src_err(
        "\
fn main () {
    print(1);
}
",
        AnalyzerError::InvalidFunctionCallArguments {
            expected: "String".to_string(),
            found: vec![Type::Int],
            span: SourceIndex::span((1, 4), (1, 12)),
        },
    );
    src_err(
        "\
fn main () {
    let a: Int = a(false, 1.0,);
}

fn a(a: Unit, b: Int, c: Float, d: Bool, e: String, f: [Int]): Int {
    return b;
}
",
        AnalyzerError::InvalidFunctionCallArguments {
            expected: "Unit, Int, Float, Bool, String, [Int]".to_string(),
            found: vec![Type::Bool, Type::Float],
            span: SourceIndex::span((1, 17), (1, 31)),
        },
    );
}

#[test]
fn array_init() {
    src_ok(
        "\
fn main() {
    [1, 2, 3, 4];
    [1; 10];
    [0; 0];
}
",
    );
    src_err(
        "fn main() { [[1, 2, 3],]; }",
        AnalyzerError::ArrayTypeInArrayInit(SourceIndex::span((0, 13), (0, 22))),
    );
    src_err(
        "fn main() { [0; 0.0]; }",
        AnalyzerError::ArrayInitRepeatNotInt(Type::Float, SourceIndex::span((0, 16), (0, 19))),
    );
    src_err(
        "fn main() { []; }",
        AnalyzerError::ArrayInitListEmpty(SourceIndex::span((0, 12), (0, 14))),
    );
    src_err(
        "fn main() { [1, 2, 3.0]; }",
        AnalyzerError::ArrayInitListConflictingTypes {
            first: Type::Int,
            other: Type::Float,
            span: SourceIndex::span((0, 19), (0, 22)),
        },
    );
}

#[test]
fn returning_function() {
    src_ok(
        "\
fn main() {}

fn a(): Int {
    let x = 1;
    return x;
}
",
    );
    src_ok(
        "\
fn main() {}

fn a(): Int {
    let x = 1;
    panic();
}
",
    );
    src_err(
        "\
fn main () {}

fn a(): Int {
    let a = 1;
    2 + 2;
}
",
        AnalyzerError::InvalidLastFnStatement {
            expected_type: Type::Int,
            span: SourceIndex::span((2, 0), (5, 1)),
        },
    );
}

#[test]
fn test_program() {
    let code = analyze_src(
        "\
fn main() {
    if false {
        return ();
    } elif true {
        test(10);
    }
}

fn test(a: Int): Int {
    let acc = [0, 1];
    let i = 0;
    while i < a - 1 {
        push(acc, acc[i] + acc[i + 1]);
        i = i + 1.0 as Int;
    }
    return acc[a];
}
",
    )
    .unwrap();
    assert_eq!(
        code,
        IRCode::new(
            [
                (
                    "main".to_string(),
                    UserDefinedFunction {
                        arguments: vec![],
                        body: vec![Statement::If {
                            branches: vec![
                                (
                                    Expression::Value(Literal::Bool(false)),
                                    vec![Statement::Return(Expression::Value(Literal::Unit)),]
                                ),
                                (
                                    Expression::Value(Literal::Bool(true)),
                                    vec![Statement::Expression(Expression::FunctionCall {
                                        function: CallableFunction::UserDefined("test".to_string()),
                                        arguments: vec![Expression::Value(Literal::Int(10))],
                                        span: SourceIndex::span((4, 8), (4, 16)),
                                    })]
                                )
                            ],
                            else_branch: vec![]
                        },],
                    }
                ),
                (
                    "test".to_string(),
                    UserDefinedFunction {
                        arguments: vec!["a".to_string()],
                        body: vec![
                            Statement::Declaration {
                                identifier: "acc".to_string(),
                                value: Expression::FunctionCall {
                                    function: CallableFunction::ArrayInit(ArrayInitFunction::List),
                                    arguments: vec![
                                        Expression::Value(Literal::Int(0)),
                                        Expression::Value(Literal::Int(1))
                                    ],
                                    span: SourceIndex::span((9, 14), (9, 20)),
                                }
                            },
                            Statement::Declaration {
                                identifier: "i".to_string(),
                                value: Expression::Value(Literal::Int(0)),
                            },
                            Statement::While {
                                condition: Expression::FunctionCall {
                                    function: CallableFunction::Operator(OperatorFunction::Less),
                                    arguments: vec![
                                        Expression::Variable("i".to_string()),
                                        Expression::FunctionCall {
                                            function: CallableFunction::Operator(
                                                OperatorFunction::Subtract
                                            ),
                                            arguments: vec![
                                                Expression::Variable("a".to_string()),
                                                Expression::Value(Literal::Int(1)),
                                            ],
                                            span: SourceIndex::span((11, 14), (11, 19))
                                        }
                                    ],
                                    span: SourceIndex::span((11, 10), (11, 19)),
                                },
                                body: vec![
                                    Statement::Expression(Expression::FunctionCall {
                                        function: CallableFunction::BuiltIn(BuiltInFunction::Push),
                                        arguments: vec![
                                            Expression::Variable("acc".to_string()),
                                            Expression::FunctionCall {
                                                function: CallableFunction::Operator(
                                                    OperatorFunction::Add
                                                ),
                                                arguments: vec![
                                                    Expression::FunctionCall {
                                                        function: CallableFunction::ArrayIndex(
                                                            ArrayIndexFunction
                                                        ),
                                                        arguments: vec![
                                                            Expression::Variable("acc".to_string()),
                                                            Expression::Variable("i".to_string()),
                                                        ],
                                                        span: SourceIndex::span((12, 18), (12, 24)),
                                                    },
                                                    Expression::FunctionCall {
                                                        function: CallableFunction::ArrayIndex(
                                                            ArrayIndexFunction
                                                        ),
                                                        arguments: vec![
                                                            Expression::Variable("acc".to_string()),
                                                            Expression::FunctionCall {
                                                                function:
                                                                    CallableFunction::Operator(
                                                                        OperatorFunction::Add
                                                                    ),
                                                                arguments: vec![
                                                                    Expression::Variable(
                                                                        "i".to_string()
                                                                    ),
                                                                    Expression::Value(
                                                                        Literal::Int(1)
                                                                    ),
                                                                ],
                                                                span: SourceIndex::span(
                                                                    (12, 31),
                                                                    (12, 36)
                                                                ),
                                                            }
                                                        ],
                                                        span: SourceIndex::span((12, 27), (12, 37)),
                                                    },
                                                ],
                                                span: SourceIndex::span((12, 18), (12, 37))
                                            }
                                        ],
                                        span: SourceIndex::span((12, 8), (12, 38)),
                                    }),
                                    Statement::Expression(Expression::FunctionCall {
                                        function: CallableFunction::Assignment(
                                            AssignmentFunction::Variable
                                        ),
                                        arguments: vec![
                                            Expression::Value(Literal::String("i".to_string())),
                                            Expression::FunctionCall {
                                                function: CallableFunction::Operator(
                                                    OperatorFunction::Add
                                                ),
                                                arguments: vec![
                                                    Expression::Variable("i".to_string()),
                                                    Expression::FunctionCall {
                                                        function: CallableFunction::Cast(
                                                            CastFunction::FloatToInt
                                                        ),
                                                        arguments: vec![Expression::Value(
                                                            Literal::Float(1.0)
                                                        ),],
                                                        span: SourceIndex::span((13, 16), (13, 26))
                                                    }
                                                ],
                                                span: SourceIndex::span((13, 12), (13, 26)),
                                            },
                                        ],
                                        span: SourceIndex::span((13, 8), (13, 26))
                                    })
                                ],
                            },
                            Statement::Return(Expression::FunctionCall {
                                function: CallableFunction::ArrayIndex(ArrayIndexFunction),
                                arguments: vec![
                                    Expression::Variable("acc".to_string()),
                                    Expression::Variable("a".to_string())
                                ],
                                span: SourceIndex::span((15, 11), (15, 17)),
                            })
                        ],
                    }
                ),
            ]
            .into()
        )
    )
}

#[test]
fn example_program_analyzes_ok() {
    src_ok(include_str!("../../example.ar"));
}
