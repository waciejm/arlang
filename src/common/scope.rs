use std::collections::HashMap;
use std::fmt::Debug;

/// A scope stack for the interpreter/analyzer.
/// Consists of a vector of function scopes.
/// A function scope consists of a vector of block scopes.
/// A block scope is a hash map from identifiers to values.
#[derive(Debug)]
pub struct ScopeStack<T: Debug> {
    stack: Vec<Vec<HashMap<String, T>>>,
}

impl<T: Debug> ScopeStack<T> {
    pub fn new() -> Self {
        Self { stack: Vec::new() }
    }

    /// Declares a variable in the current block scope.
    pub fn declare_variable(&mut self, identifier: String, value: T) {
        self.stack
            .last_mut()
            .unwrap()
            .last_mut()
            .unwrap()
            .insert(identifier, value);
    }

    /// Checks if there is a variable in the current function scope.
    /// Panics if there is no function scope.
    pub fn has_variable(&self, identifier: &str) -> bool {
        self.stack
            .last()
            .unwrap()
            .iter()
            .any(|h| h.contains_key(identifier))
    }

    /// Gets a shared reference to the value of a variable under given identifier.
    /// Panics if a variable with a given identifier doesn't currently exist.
    pub fn get_variable(&self, identifier: &str) -> &T {
        match self
            .stack
            .last()
            .unwrap()
            .iter()
            .rev()
            .find(|h| h.contains_key(identifier))
        {
            Some(h) => h.get(identifier).unwrap(),
            None => panic!("variable {} not found in current scope", identifier),
        }
    }

    /// Gets a shared reference to the value of a variable under given identifier.
    /// Panics if a variable with a given identifier doesn't currently exist.
    pub fn get_variable_mut(&mut self, identifier: &str) -> &mut T {
        match self
            .stack
            .last_mut()
            .unwrap()
            .iter_mut()
            .rev()
            .find(|h| h.contains_key(identifier))
        {
            Some(h) => h.get_mut(identifier).unwrap(),
            None => panic!("variable {} not found in current scope", identifier),
        }
    }

    /// Opens a function scope.
    /// Call before executing a function.
    pub fn open_function(&mut self) {
        self.stack.push(Vec::new());
        self.stack.last_mut().unwrap().push(HashMap::new());
    }

    /// Closes a function scope.
    /// Call after executing a function.
    /// Panics if there are no function scopes on the stack.
    pub fn close_function(&mut self) {
        self.stack.pop();
    }

    /// Opens a block scope.
    /// Call before executing an if, while etc. block.
    pub fn open_block(&mut self) {
        self.stack.last_mut().unwrap().push(HashMap::new());
    }

    /// Closes a block scope.
    /// Call after executing an if, while etc. block.
    /// Panics if there are no block scopes in the current function scope.
    pub fn close_block(&mut self) {
        self.stack.last_mut().unwrap().pop();
    }
}
