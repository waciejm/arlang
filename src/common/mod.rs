use std::fmt::Display;

pub mod scope;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Type {
    /// `Unit`
    Unit,
    /// `[Unit]`
    UnitArray,
    /// `Int`
    Int,
    /// `[Int]`
    IntArray,
    /// `Float`
    Float,
    /// `[Float]`
    FloatArray,
    /// `Bool`
    Bool,
    /// `[Bool]`
    BoolArray,
    /// `String`
    String,
    /// `[String]`
    StringArray,
}

impl Type {
    pub fn array_of(&self) -> Option<Self> {
        match self {
            Type::Unit => Some(Type::UnitArray),
            Type::Int => Some(Type::IntArray),
            Type::Float => Some(Type::FloatArray),
            Type::Bool => Some(Type::BoolArray),
            Type::String => Some(Type::StringArray),
            _ => None,
        }
    }

    pub fn array_inner(&self) -> Option<Self> {
        match self {
            Type::UnitArray => Some(Type::Unit),
            Type::IntArray => Some(Type::Int),
            Type::FloatArray => Some(Type::Float),
            Type::BoolArray => Some(Type::Bool),
            Type::StringArray => Some(Type::String),
            _ => None,
        }
    }

    pub fn slice_to_string(vec: &[Self]) -> String {
        let mut result = String::new();
        for i in 0..vec.len() {
            result += vec[i].pretty();
            if i < vec.len() - 1 {
                result += ", ";
            }
        }
        result
    }

    fn pretty(&self) -> &'static str {
        match self {
            Type::Unit => "Unit",
            Type::UnitArray => "[Unit]",
            Type::Int => "Int",
            Type::IntArray => "[Int]",
            Type::Float => "Float",
            Type::FloatArray => "[Float]",
            Type::Bool => "Bool",
            Type::BoolArray => "[Bool]",
            Type::String => "String",
            Type::StringArray => "[String]",
        }
    }
}

impl Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.pretty())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Unit,
    Int(i64),
    Float(f64),
    String(String),
    Bool(bool),
}

impl Literal {
    pub fn type_of_self(&self) -> Type {
        match self {
            Literal::Unit => Type::Unit,
            Literal::Int(_) => Type::Int,
            Literal::Float(_) => Type::Float,
            Literal::String(_) => Type::String,
            Literal::Bool(_) => Type::Bool,
        }
    }
}
