use std::{
    cell::RefCell,
    io::Write,
    ops::{ControlFlow, Range},
    rc::Rc,
};

use crate::{
    common::scope::ScopeStack,
    interpreter::ir::{
        ArrayIndexFunction, ArrayInitFunction, AssignmentFunction, BuiltInFunction,
        CallableFunction, CastFunction, Expression, IRCode, OperatorFunction, Statement,
        UserDefinedFunction, Value,
    },
    source::{SourceError, SourceIndex},
};

pub mod ir;

/// The interpreter.
/// Takes an [IRCode](ir::IRCode) produced by the analyzer and
/// executes it with the assumption, that it is correct.
/// Can output interpreted code output to anything [Write](std::io::Write).
#[derive(Debug)]
pub struct Interpreter<'a, Output: Write> {
    code: &'a IRCode,
    scope: ScopeStack<Value>,
    output: Output,
}

impl<'a, Output: Write> Interpreter<'a, Output> {
    pub fn new(code: &'a IRCode, output: Output) -> Self {
        Self {
            code,
            scope: ScopeStack::new(),
            output,
        }
    }

    pub fn run(mut self) -> Result<(), RuntimeError> {
        self.execute_user_defined_fn("main", &[])?;
        Ok(())
    }
}

impl<Output: Write> Interpreter<'_, Output> {
    fn execute_user_defined_fn(
        &mut self,
        function: &str,
        arguments: &[Value],
    ) -> Result<Value, RuntimeError> {
        let function = match self.code.functions.get(function) {
            Some(f) => f,
            None => panic!("function {} not in available functions", function),
        };
        self.scope.open_function();
        self.push_arguments_to_scope(function, arguments);
        let result = self.execute_block(&function.body);
        self.scope.close_function();
        match result {
            ControlFlow::Break(ret) => ret,
            ControlFlow::Continue(()) => Ok(Value::Unit),
        }
    }

    fn push_arguments_to_scope(&mut self, function: &UserDefinedFunction, arguments: &[Value]) {
        for (identifier, value) in function.arguments.iter().zip(arguments) {
            self.scope
                .declare_variable(identifier.clone(), value.clone());
        }
    }

    fn execute_block(&mut self, block: &[Statement]) -> ControlFlow<Result<Value, RuntimeError>> {
        self.scope.open_block();

        for statement in block {
            self.execute_statement(statement)?;
        }

        self.scope.close_block();
        ControlFlow::Continue(())
    }

    fn execute_statement(
        &mut self,
        statement: &Statement,
    ) -> ControlFlow<Result<Value, RuntimeError>> {
        match statement {
            Statement::Declaration { identifier, value } => {
                match self.calculate_expression(value) {
                    Ok(value) => {
                        self.scope.declare_variable(identifier.clone(), value);
                        ControlFlow::Continue(())
                    }
                    Err(e) => ControlFlow::Break(Err(e)),
                }
            }
            Statement::Return(expression) => {
                ControlFlow::Break(self.calculate_expression(expression))
            }
            Statement::If {
                branches,
                else_branch,
            } => {
                for (expression, block) in branches {
                    match self.calculate_expression(expression) {
                        Ok(Value::Bool(true)) => {
                            return self.execute_block(block);
                        }
                        Ok(Value::Bool(false)) => {}
                        Err(e) => {
                            return ControlFlow::Break(Err(e));
                        }
                        Ok(x) => {
                            panic!("if/elif condition value not bool: {:?}", x);
                        }
                    }
                }
                self.execute_block(else_branch)
            }
            Statement::While { condition, body } => loop {
                match self.calculate_expression(condition) {
                    Ok(Value::Bool(true)) => {
                        self.execute_block(body)?;
                    }
                    Ok(Value::Bool(false)) => break ControlFlow::Continue(()),
                    Err(e) => break ControlFlow::Break(Err(e)),
                    Ok(x) => panic!("while condition value not bool: {:?}", x),
                }
            },
            Statement::Expression(expression) => match self.calculate_expression(expression) {
                Ok(_) => ControlFlow::Continue(()),
                Err(e) => ControlFlow::Break(Err(e)),
            },
        }
    }

    fn calculate_expression(&mut self, expression: &Expression) -> Result<Value, RuntimeError> {
        match expression {
            Expression::FunctionCall {
                function,
                arguments,
                span,
            } => {
                let mut args = Vec::new();
                for argument in arguments {
                    match self.calculate_expression(argument) {
                        Ok(arg) => args.push(arg),
                        Err(e) => return Err(e),
                    }
                }
                match function {
                    CallableFunction::BuiltIn(function) => {
                        self.execute_built_in_fn(*function, &args, span)
                    }
                    CallableFunction::Operator(function) => {
                        Self::execute_operator_fn(*function, &args, span)
                    }
                    CallableFunction::Cast(function) => Ok(Self::execute_cast_fn(*function, &args)),
                    CallableFunction::ArrayIndex(function) => {
                        Self::execute_array_index_fn(*function, &args, span)
                    }
                    CallableFunction::ArrayInit(function) => {
                        Self::execute_array_init_fn(*function, &args)
                    }
                    CallableFunction::Assignment(function) => {
                        self.execute_assignment_fn(*function, &args, span)
                    }
                    CallableFunction::UserDefined(function) => {
                        self.execute_user_defined_fn(function, &args)
                    }
                }
            }
            Expression::Variable(identifier) => Ok(self.scope.get_variable(identifier).clone()),
            Expression::Value(value) => Ok(value.clone().into()),
        }
    }
}

impl<Output: Write> Interpreter<'_, Output> {
    fn execute_built_in_fn(
        &mut self,
        function: BuiltInFunction,
        arguments: &[Value],
        span: &Range<SourceIndex>,
    ) -> Result<Value, RuntimeError> {
        match function {
            BuiltInFunction::Print => match arguments {
                [Value::String(string)] => match write!(self.output, "{}", string) {
                    Ok(()) => Ok(Value::Unit),
                    Err(error) => Err(RuntimeError::WriteToOutputFailed {
                        error,
                        span: span.clone(),
                    }),
                },
                _ => function.error(arguments),
            },
            BuiltInFunction::Panic => match arguments {
                [] => Err(RuntimeError::Panic(span.clone())),
                _ => function.error(arguments),
            },
            BuiltInFunction::Size => match arguments {
                [Value::Array(array)] => Ok(Value::Int(array.borrow().len() as i64)),
                _ => function.error(arguments),
            },
            BuiltInFunction::Push => match arguments {
                [_, Value::Array(_)] => function.error(arguments),
                [Value::Array(array), value] => {
                    array.borrow_mut().push(value.clone());
                    Ok(Value::Unit)
                }
                _ => function.error(arguments),
            },
            BuiltInFunction::Pop => match arguments {
                [Value::Array(array)] => {
                    array.borrow_mut().pop();
                    Ok(Value::Unit)
                }
                _ => function.error(arguments),
            },
            BuiltInFunction::Insert => match arguments {
                [_, _, Value::Array(_)] => function.error(arguments),
                [Value::Array(array), Value::Int(index), value] => {
                    let mut array = array.borrow_mut();
                    if (array.len()) < *index as usize {
                        Err(RuntimeError::ArrayIndexOutOfRange {
                            size: array.len(),
                            index: *index,
                            span: span.clone(),
                        })
                    } else {
                        array.insert(*index as usize, value.clone());
                        Ok(Value::Unit)
                    }
                }
                _ => function.error(arguments),
            },
            BuiltInFunction::Remove => match arguments {
                [Value::Array(array), Value::Int(index)] => {
                    let mut array = array.borrow_mut();
                    if (array.len()) <= *index as usize {
                        Err(RuntimeError::ArrayIndexOutOfRange {
                            size: array.len(),
                            index: *index,
                            span: span.clone(),
                        })
                    } else {
                        array.remove(*index as usize);
                        Ok(Value::Unit)
                    }
                }
                _ => function.error(arguments),
            },
        }
    }

    fn execute_operator_fn(
        function: OperatorFunction,
        arguments: &[Value],
        span: &Range<SourceIndex>,
    ) -> Result<Value, RuntimeError> {
        match function {
            OperatorFunction::Negate => match arguments {
                [Value::Int(int)] => Ok(Value::Int(-int)),
                [Value::Float(float)] => Ok(Value::Float(-float)),
                [Value::Bool(bool)] => Ok(Value::Bool(!bool)),
                _ => function.error(arguments),
            },
            OperatorFunction::Add => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Int(a + b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Float(a + b)),
                [Value::String(a), Value::String(b)] => {
                    Ok(Value::String(format!("{}{}", a, b).into()))
                }
                [Value::Array(a), Value::Array(b)] => {
                    let mut new = a.borrow().clone();
                    new.extend(b.borrow().iter().cloned());
                    Ok(Value::Array(Rc::new(RefCell::new(new))))
                }
                _ => function.error(arguments),
            },
            OperatorFunction::Subtract => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Int(a - b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Float(a - b)),
                _ => function.error(arguments),
            },
            OperatorFunction::Multiply => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Int(a * b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Float(a * b)),
                _ => function.error(arguments),
            },
            OperatorFunction::Divide => match arguments {
                [Value::Int(a), Value::Int(b)] => {
                    if *b != 0 {
                        Ok(Value::Int(a / b))
                    } else {
                        Err(RuntimeError::DivisionByZero(span.clone()))
                    }
                }
                [Value::Float(a), Value::Float(b)] => Ok(Value::Float(a / b)),
                _ => function.error(arguments),
            },
            OperatorFunction::Less => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Bool(a < b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Bool(a < b)),
                _ => function.error(arguments),
            },
            OperatorFunction::LessEqual => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Bool(a <= b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Bool(a <= b)),
                _ => function.error(arguments),
            },
            OperatorFunction::More => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Bool(a > b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Bool(a > b)),
                _ => function.error(arguments),
            },
            OperatorFunction::MoreEqual => match arguments {
                [Value::Int(a), Value::Int(b)] => Ok(Value::Bool(a >= b)),
                [Value::Float(a), Value::Float(b)] => Ok(Value::Bool(a >= b)),
                _ => function.error(arguments),
            },
            OperatorFunction::Equal => match arguments {
                [a @ Value::Int(_), b @ Value::Int(_)] => Ok(Value::Bool(a == b)),
                [a @ Value::Float(_), b @ Value::Float(_)] => Ok(Value::Bool(a == b)),
                [a @ Value::String(_), b @ Value::String(_)] => Ok(Value::Bool(a == b)),
                [a @ Value::Bool(_), b @ Value::Bool(_)] => Ok(Value::Bool(a == b)),
                [a @ Value::Array(_), b @ Value::Array(_)] => Ok(Value::Bool(a == b)),
                _ => function.error(arguments),
            },
            OperatorFunction::NotEqual => match arguments {
                [a @ Value::Int(_), b @ Value::Int(_)] => Ok(Value::Bool(a != b)),
                [a @ Value::Float(_), b @ Value::Float(_)] => Ok(Value::Bool(a != b)),
                [a @ Value::String(_), b @ Value::String(_)] => Ok(Value::Bool(a != b)),
                [a @ Value::Bool(_), b @ Value::Bool(_)] => Ok(Value::Bool(a != b)),
                [a @ Value::Array(_), b @ Value::Array(_)] => Ok(Value::Bool(a != b)),
                _ => function.error(arguments),
            },
            OperatorFunction::And => match arguments {
                [Value::Bool(a), Value::Bool(b)] => Ok(Value::Bool(*a && *b)),
                _ => function.error(arguments),
            },
            OperatorFunction::Or => match arguments {
                [Value::Bool(a), Value::Bool(b)] => Ok(Value::Bool(*a || *b)),
                _ => function.error(arguments),
            },
        }
    }

    fn execute_cast_fn(function: CastFunction, arguments: &[Value]) -> Value {
        match function {
            CastFunction::IntToFloat => match arguments {
                [Value::Int(int)] => Value::Float(*int as f64),
                _ => function.error(arguments),
            },
            CastFunction::IntToString => match arguments {
                [Value::Int(int)] => Value::String(int.to_string().into()),
                _ => function.error(arguments),
            },
            CastFunction::FloatToInt => match arguments {
                [Value::Float(float)] => Value::Int(*float as i64),
                _ => function.error(arguments),
            },
            CastFunction::FloatToString => match arguments {
                [Value::Float(float)] => Value::String(float.to_string().into()),
                _ => function.error(arguments),
            },
            CastFunction::BoolToInt => match arguments {
                [Value::Bool(bool)] => Value::Int(if *bool { 1 } else { 0 }),
                _ => function.error(arguments),
            },
            CastFunction::BoolToFloat => match arguments {
                [Value::Bool(bool)] => Value::Float(if *bool { 1.0 } else { 0.0 }),
                _ => function.error(arguments),
            },
            CastFunction::BoolToString => match arguments {
                [Value::Bool(bool)] => Value::String(bool.to_string().into()),
                _ => function.error(arguments),
            },
            CastFunction::Identity => match arguments {
                [v] => v.clone(),
                _ => function.error(arguments),
            },
        }
    }

    fn execute_array_index_fn(
        function: ArrayIndexFunction,
        arguments: &[Value],
        span: &Range<SourceIndex>,
    ) -> Result<Value, RuntimeError> {
        match arguments {
            [Value::Array(array), Value::Int(index)] => {
                let array = array.borrow();
                if let Some(value) = array.get(*index as usize) {
                    Ok(value.clone())
                } else {
                    Err(RuntimeError::ArrayIndexOutOfRange {
                        size: array.len(),
                        index: *index,
                        span: span.clone(),
                    })
                }
            }
            _ => function.error(arguments),
        }
    }

    fn execute_array_init_fn(
        function: ArrayInitFunction,
        arguments: &[Value],
    ) -> Result<Value, RuntimeError> {
        let mut array = Vec::new();
        match function {
            ArrayInitFunction::Repeat => match arguments {
                [value, Value::Int(count)] => {
                    for _ in 0..*count {
                        array.push(value.clone());
                    }
                }
                _ => function.error(arguments),
            },
            ArrayInitFunction::List => {
                for value in arguments {
                    array.push(value.clone());
                }
            }
        }
        Ok(Value::Array(Rc::new(RefCell::new(array))))
    }

    fn execute_assignment_fn(
        &mut self,
        function: AssignmentFunction,
        arguments: &[Value],
        span: &Range<SourceIndex>,
    ) -> Result<Value, RuntimeError> {
        match function {
            AssignmentFunction::Variable => match arguments {
                [Value::String(identifier), value] => {
                    *self.scope.get_variable_mut(identifier) = value.clone();
                    Ok(Value::Unit)
                }
                _ => function.error(arguments),
            },
            AssignmentFunction::Array => match arguments {
                [Value::Array(array), Value::Int(index), value] => {
                    let mut array = array.borrow_mut();
                    match array.get_mut(*index as usize) {
                        Some(val) => {
                            *val = value.clone();
                            Ok(Value::Unit)
                        }
                        None => Err(RuntimeError::ArrayIndexOutOfRange {
                            index: *index,
                            size: array.len(),
                            span: span.clone(),
                        }),
                    }
                }
                _ => function.error(arguments),
            },
        }
    }
}

#[derive(Debug)]
pub enum RuntimeError {
    Panic(Range<SourceIndex>),
    DivisionByZero(Range<SourceIndex>),
    ArrayIndexOutOfRange {
        size: usize,
        index: i64,
        span: Range<SourceIndex>,
    },
    WriteToOutputFailed {
        error: std::io::Error,
        span: Range<SourceIndex>,
    },
}

impl From<RuntimeError> for SourceError {
    fn from(error: RuntimeError) -> Self {
        match error {
            RuntimeError::Panic(span) => {
                SourceError::new(span, "RUNTIME ERROR: Panic.".to_string())
            }
            RuntimeError::DivisionByZero(span) => {
                SourceError::new(span, "RUNTIME ERROR: Division by zero.".to_string())
            }
            RuntimeError::ArrayIndexOutOfRange { index, size, span } => SourceError::new(
                span,
                format!(
                    "RUNTIME ERROR: Array index out of range (size: {}, index: {}).",
                    size, index
                ),
            ),
            RuntimeError::WriteToOutputFailed { error, span } => SourceError::new(
                span,
                format!("RUNTIME ERROR: Failed to write to output: {}", error,),
            ),
        }
    }
}

#[cfg(test)]
mod test;
