use std::{cell::RefCell, collections::HashMap, ops::Range, rc::Rc};

use crate::{
    common::{Literal, Type},
    parser::syntax_tree::operator::{BinaryOperator, UnaryOperator},
    source::SourceIndex,
};

/// Intermidiate representation of the code to be interpreted.
#[derive(Debug, PartialEq)]
pub struct IRCode {
    pub functions: HashMap<String, UserDefinedFunction>,
}

impl IRCode {
    pub fn new(functions: HashMap<String, UserDefinedFunction>) -> Self {
        Self { functions }
    }
}

#[derive(Debug, PartialEq)]
pub struct UserDefinedFunction {
    pub arguments: Vec<String>,
    pub body: Vec<Statement>,
}

#[derive(Debug, PartialEq)]
pub enum Statement {
    Declaration {
        identifier: String,
        value: Expression,
    },
    Return(Expression),
    If {
        branches: Vec<(Expression, Vec<Statement>)>,
        else_branch: Vec<Statement>,
    },
    While {
        condition: Expression,
        body: Vec<Statement>,
    },
    Expression(Expression),
}

#[derive(Debug, PartialEq)]
pub enum Expression {
    FunctionCall {
        function: CallableFunction,
        arguments: Vec<Expression>,
        span: Range<SourceIndex>,
    },
    Variable(String),
    Value(Literal),
}

#[derive(Debug, PartialEq)]
pub enum CallableFunction {
    BuiltIn(BuiltInFunction),
    Operator(OperatorFunction),
    Cast(CastFunction),
    ArrayIndex(ArrayIndexFunction),
    ArrayInit(ArrayInitFunction),
    Assignment(AssignmentFunction),
    UserDefined(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Unit,
    Int(i64),
    Float(f64),
    String(Rc<str>),
    Bool(bool),
    Array(Rc<RefCell<Vec<Value>>>),
}

impl From<Literal> for Value {
    fn from(literal: Literal) -> Self {
        match literal {
            Literal::Unit => Value::Unit,
            Literal::Int(int) => Value::Int(int),
            Literal::Float(float) => Value::Float(float),
            Literal::String(string) => Value::String(string.into()),
            Literal::Bool(bool) => Value::Bool(bool),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BuiltInFunction {
    Print,
    Panic,
    Size,
    Push,
    Pop,
    Insert,
    Remove,
}

impl BuiltInFunction {
    pub const NAMES: &'static [&'static str] =
        &["print", "panic", "size", "push", "pop", "insert", "remove"];

    pub fn error(self, arguments: &[Value]) -> ! {
        panic!(
            "executed built in function {:?} with wrong arguments:\n{:?}",
            self, arguments
        )
    }

    pub fn get_by_identifier(identifier: &str) -> Self {
        match identifier {
            "print" => Self::Print,
            "panic" => Self::Panic,
            "size" => Self::Size,
            "push" => Self::Push,
            "pop" => Self::Pop,
            "insert" => Self::Insert,
            "remove" => Self::Remove,
            _ => panic!(),
        }
    }

    pub fn are_expected_arguments(&self, args: &[Type]) -> bool {
        match self {
            BuiltInFunction::Print => matches!(args, [Type::String]),
            BuiltInFunction::Panic => matches!(args, []),
            BuiltInFunction::Size => {
                if let [t] = args {
                    t.array_inner().is_some()
                } else {
                    false
                }
            }
            BuiltInFunction::Push => {
                if let [array, value] = args {
                    array.array_inner() == Some(*value)
                } else {
                    false
                }
            }
            BuiltInFunction::Pop => {
                if let [t] = args {
                    t.array_inner().is_some()
                } else {
                    false
                }
            }
            BuiltInFunction::Insert => {
                if let [array, index, value] = args {
                    array.array_inner() == Some(*value) && *index == Type::Int
                } else {
                    false
                }
            }
            BuiltInFunction::Remove => {
                if let [array, index] = args {
                    array.array_inner().is_some() && *index == Type::Int
                } else {
                    false
                }
            }
        }
    }

    pub fn expected_arguments(&self) -> String {
        match self {
            BuiltInFunction::Print => "String".to_string(),
            BuiltInFunction::Panic => String::new(),
            BuiltInFunction::Size => "[T]".to_string(),
            BuiltInFunction::Push => "[T], T".to_string(),
            BuiltInFunction::Pop => "[T]".to_string(),
            BuiltInFunction::Insert => "[T], Int, T".to_string(),
            BuiltInFunction::Remove => "[T], Int".to_string(),
        }
    }

    pub fn return_type(&self) -> Type {
        match self {
            BuiltInFunction::Print => Type::Unit,
            BuiltInFunction::Panic => Type::Unit,
            BuiltInFunction::Size => Type::Int,
            BuiltInFunction::Push => Type::Unit,
            BuiltInFunction::Pop => Type::Unit,
            BuiltInFunction::Insert => Type::Unit,
            BuiltInFunction::Remove => Type::Unit,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum OperatorFunction {
    Negate,
    Add,
    Subtract,
    Multiply,
    Divide,
    Less,
    LessEqual,
    More,
    MoreEqual,
    Equal,
    NotEqual,
    And,
    Or,
}

impl OperatorFunction {
    pub fn error(self, arguments: &[Value]) -> ! {
        panic!(
            "executed operator function {:?} with wrong arguments:\n{:?}",
            self, arguments
        )
    }
}

impl From<UnaryOperator> for OperatorFunction {
    fn from(operator: UnaryOperator) -> Self {
        match operator {
            UnaryOperator::Not => OperatorFunction::Negate,
            UnaryOperator::Minus => OperatorFunction::Negate,
        }
    }
}

impl From<BinaryOperator> for OperatorFunction {
    fn from(operator: BinaryOperator) -> Self {
        match operator {
            BinaryOperator::Plus => OperatorFunction::Add,
            BinaryOperator::Minus => OperatorFunction::Subtract,
            BinaryOperator::Multiply => OperatorFunction::Multiply,
            BinaryOperator::Divide => OperatorFunction::Divide,
            BinaryOperator::Less => OperatorFunction::Less,
            BinaryOperator::LessEqual => OperatorFunction::LessEqual,
            BinaryOperator::More => OperatorFunction::More,
            BinaryOperator::MoreEqual => OperatorFunction::MoreEqual,
            BinaryOperator::Equal => OperatorFunction::Equal,
            BinaryOperator::NotEqual => OperatorFunction::NotEqual,
            BinaryOperator::Assignment => panic!(),
            BinaryOperator::And => OperatorFunction::And,
            BinaryOperator::Or => OperatorFunction::Or,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum CastFunction {
    IntToFloat,
    IntToString,
    FloatToInt,
    FloatToString,
    BoolToInt,
    BoolToFloat,
    BoolToString,
    Identity,
}

impl CastFunction {
    pub fn valid_casts() -> &'static str {
        "Int as Float, 
Int as String, 
Float as Int, 
Float as String, 
Bool as Int, 
Bool as Float, 
Bool as String, 
T as T"
    }

    pub fn error(self, arguments: &[Value]) -> ! {
        panic!(
            "executed cast function {:?} with wrong arguments:\n{:?}",
            self, arguments
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct ArrayIndexFunction;

impl ArrayIndexFunction {
    pub fn error(self, arguments: &[Value]) -> ! {
        panic!(
            "executed array index function {:?} with wrong arguments:\n{:?}",
            self, arguments
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ArrayInitFunction {
    Repeat,
    List,
}

impl ArrayInitFunction {
    pub fn error(self, arguments: &[Value]) -> ! {
        panic!(
            "executed array init function {:?} with wrong arguments:\n{:?}",
            self, arguments
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AssignmentFunction {
    Variable,
    Array,
}

impl AssignmentFunction {
    pub fn error(self, arguments: &[Value]) -> ! {
        panic!(
            "executed assignment function {:?} with wrong arguments:\n{:?}",
            self, arguments
        )
    }
}
