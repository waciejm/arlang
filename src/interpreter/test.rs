use std::{io::Write, ops::Range};

use crate::{interpreter::RuntimeError, source::SourceIndex};

fn run_code<O: Write>(src: &str, output: O) -> Result<(), RuntimeError> {
    let source_iter = crate::source::SourceIter::new(src.as_bytes());
    let tokenizer = crate::tokenizer::Tokenizer::new(source_iter);
    let parser = crate::parser::Parser::new(tokenizer);
    let ast = parser.parse().unwrap();
    let analyzer = crate::analyzer::SemanticAnalyzer::new(ast, false);
    let ircode = analyzer.analyze().unwrap();
    let interpreter = crate::interpreter::Interpreter::new(&ircode, output);
    interpreter.run()
}

fn code_output(src: &str) -> String {
    let mut output = Vec::new();
    run_code(src, &mut output).unwrap();
    String::from_utf8(output).unwrap()
}

#[test]
fn example_output() {
    assert_eq!(
        &code_output(include_str!("../../example.ar")),
        "\
8 <- Float
Hello World !
15
1 3 4
256
1 1 2 3 5 8 13 21 34 55
",
    );
}

#[test]
fn builtin_functions() {
    // print
    assert_eq!(
        &code_output("fn main() { print(\"test string\"); }"),
        "test string",
    );
    // size
    assert_eq!(
        code_output("fn main() { let a = [1, 2, 3]; print(size(a) as String); }"),
        3.to_string(),
    );
    // push
    assert_eq!(
        code_output("fn main() { let a = [0; 0]; push(a, 13); print(a[0] as String); }"),
        13.to_string(),
    );
    // pop
    assert_eq!(
        code_output("fn main() { let a = [0; 3]; pop(a); print(size(a) as String); }"),
        2.to_string(),
    );
    // insert
    assert_eq!(
        code_output("fn main() { let a = [1, 2, 3]; insert(a, 2, 10); print(a[2] as String); }"),
        10.to_string(),
    );
    // remove
    assert_eq!(
        code_output("fn main() { let a = [1, 2, 3]; remove(a, 1); print(a[1] as String); }"),
        3.to_string(),
    )
}

#[test]
fn addition() {
    assert_eq!(
        code_output("fn main() { print((15 + 19) as String); }"),
        (15 + 19).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.0 + 16.5) as String); }"),
        (13.0 + 16.5).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print(\"as\" + \"df\"); }"),
        "as".to_string() + "df",
    );
    assert_eq!(
        code_output("fn main() { print(([1, 2] + [3, 4] == [1, 2, 3, 4]) as String); }"),
        true.to_string(),
    );
}

#[test]
fn subtraction() {
    assert_eq!(
        code_output("fn main() { print((14 - 9) as String); }"),
        (14 - 9).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((23.4 - 11.3) as String); }"),
        (23.4 - 11.3).to_string(),
    );
}

#[test]
fn multiplication() {
    assert_eq!(
        code_output("fn main() { print((3 * 4) as String); }"),
        (3 * 4).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((0.6 * 21.1) as String); }"),
        (0.6 * 21.1).to_string(),
    );
}

#[test]
fn division() {
    assert_eq!(
        code_output("fn main() { print((3 / 4) as String); }"),
        (3 / 4).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((0.6 / 21.1) as String); }"),
        (0.6 / 21.1).to_string(),
    );
}

#[test]
fn less() {
    assert_eq!(
        code_output("fn main() { print((2 < 3) as String); }"),
        (2 < 3).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 < 2) as String); }"),
        (2 < 2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 < 1) as String); }"),
        (2 < 1).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 < 12.2) as String); }"),
        (13.2 < 12.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 < 13.2) as String); }"),
        (13.2 < 13.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 < 14.2) as String); }"),
        (13.2 < 14.2).to_string(),
    );
}

#[test]
fn less_equal() {
    assert_eq!(
        code_output("fn main() { print((2 <= 3) as String); }"),
        (2 <= 3).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 <= 2) as String); }"),
        (2 <= 2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 <= 1) as String); }"),
        (2 <= 1).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 <= 12.2) as String); }"),
        (13.2 <= 12.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 <= 13.2) as String); }"),
        (13.2 <= 13.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 <= 14.2) as String); }"),
        (13.2 <= 14.2).to_string(),
    );
}

#[test]
fn greater() {
    assert_eq!(
        code_output("fn main() { print((2 > 3) as String); }"),
        (2 > 3).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 > 2) as String); }"),
        (2 > 2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 > 1) as String); }"),
        (2 > 1).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 > 12.2) as String); }"),
        (13.2 > 12.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 > 13.2) as String); }"),
        (13.2 > 13.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 > 14.2) as String); }"),
        (13.2 > 14.2).to_string(),
    );
}

#[test]
fn greater_equal() {
    assert_eq!(
        code_output("fn main() { print((2 >= 3) as String); }"),
        (2 >= 3).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 >= 2) as String); }"),
        (2 >= 2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 >= 1) as String); }"),
        (2 >= 1).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 >= 12.2) as String); }"),
        (13.2 >= 12.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 >= 13.2) as String); }"),
        (13.2 >= 13.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 >= 14.2) as String); }"),
        (13.2 >= 14.2).to_string(),
    );
}

#[test]
fn equal() {
    assert_eq!(
        code_output("fn main() { print((2 == 3) as String); }"),
        (2 == 3).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 == 2) as String); }"),
        (2 == 2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 == 1) as String); }"),
        (2 == 1).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 == 12.2) as String); }"),
        (13.2 == 12.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 == 13.2) as String); }"),
        (13.2 == 13.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 == 14.2) as String); }"),
        (13.2 == 14.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true == false) as String); }"),
        (true == false).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true == true) as String); }"),
        (true == true).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false == false) as String); }"),
        (false == false).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((\"as\" == \"df\") as String); }"),
        ("as" == "df").to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((\"as\" == \"as\") as String); }"),
        ("as" == "as").to_string(),
    );
    assert_eq!(
        code_output("fn main() { print(([1, 2] == [1, 2]) as String); }"),
        ([1, 2] == [1, 2]).to_string(),
    );
}

#[test]
fn not_equal() {
    assert_eq!(
        code_output("fn main() { print((2 != 3) as String); }"),
        (2 != 3).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 != 2) as String); }"),
        (2 != 2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((2 != 1) as String); }"),
        (2 != 1).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 != 12.2) as String); }"),
        (13.2 != 12.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 != 13.2) as String); }"),
        (13.2 != 13.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((13.2 != 14.2) as String); }"),
        (13.2 != 14.2).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true != false) as String); }"),
        (true != false).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true != true) as String); }"),
        (true != true).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false != false) as String); }"),
        (false != false).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((\"as\" != \"df\") as String); }"),
        ("as" != "df").to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((\"as\" != \"as\") as String); }"),
        ("as" != "as").to_string(),
    );
    assert_eq!(
        code_output("fn main() { print(([1, 2] != [1, 2]) as String); }"),
        ([1, 2] != [1, 2]).to_string(),
    );
}

#[test]
fn assignment() {
    assert_eq!(
        code_output("fn main() { let a = 0; a = 1; print(a as String); }"),
        1.to_string(),
    );
}

#[test]
fn and() {
    assert_eq!(
        code_output("fn main() { print((true && true) as String); }"),
        (true && true).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true && false) as String); }"),
        (true && false).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false && true) as String); }"),
        (false && true).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false && false) as String); }"),
        (false && false).to_string(),
    );
}

#[test]
fn or() {
    assert_eq!(
        code_output("fn main() { print((true || true) as String); }"),
        (true || true).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true || false) as String); }"),
        (true || false).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false || true) as String); }"),
        (false || true).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false || false) as String); }"),
        (false || false).to_string(),
    );
}

#[test]
fn array_mutation() {
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = [1, 0, 1];
    a[1] = 1;
    print(a[1] as String);
}
"
        ),
        1.to_string(),
    );
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = [1, 0, 1];
    fun(a);
    print(a[1] as String);
}

fn fun(arr: [Int]) {
    arr[1] = 1;
}
"
        ),
        1.to_string(),
    );
}

#[test]
fn declarations_and_scope() {
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 1;
    if true {
        let a = 2;
        print(a as String);
    }
    print(a as String);
}
"
        ),
        2.to_string() + &1.to_string(),
    );
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 1;
    fun();
    print(a as String);
}

fn fun() {
    let a = 2;
    print(a as String);
}
"
        ),
        2.to_string() + &1.to_string(),
    );
}

#[test]
fn casts() {
    assert_eq!(
        code_output("fn main() { print((1 as Float) as String); }"),
        (1 as f64).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((1.0 as Int) as String); }"),
        (1.0 as i64).to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true as Int) as String); }"),
        1.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false as Int) as String); }"),
        0.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true as Float) as String); }"),
        1.0.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((false as Float) as String); }"),
        0.0.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((1 as Int) as String); }"),
        1.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((1.0 as Float) as String); }"),
        1.0.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((true as Bool) as String); }"),
        true.to_string(),
    );
    assert_eq!(
        code_output("fn main() { print((\"asdf\" as String) as String); }"),
        "asdf".to_string(),
    );
    assert_eq!(
        code_output("fn main() { print(([1, 2, 3] as [Int] == [1, 2, 3]) as String); }"),
        true.to_string(),
    );
}

#[test]
fn conditionals() {
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 0;
    if true {
        a = 1;
    } elif true {
        a = 2;
    } else {
        a = 3;
    }
    print(a as String);
}
"
        ),
        1.to_string(),
    );
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 0;
    if false {
        a = 1;
    } elif true {
        a = 2;
    } else {
        a = 3;
    }
    print(a as String);
}
"
        ),
        2.to_string(),
    );
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 0;
    if false {
        a = 1;
    } elif false {
        a = 2;
    } else {
        a = 3;
    }
    print(a as String);
}
"
        ),
        3.to_string(),
    );
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 0;
    while false {
        a = 1;
    }
    print(a as String);
}
"
        ),
        0.to_string(),
    );
    assert_eq!(
        code_output(
            "\
fn main() {
    let a = 0;
    let b = true;
    while b {
        b = false;
        a = 1;
    }
    print(a as String);
}
"
        ),
        1.to_string(),
    );
}

#[test]
fn runtime_error_panic() {
    assert!(matches!(
        run_code("fn main() { panic(); }", std::io::stdout()),
        Err(RuntimeError::Panic(Range {
            start: SourceIndex { line: 0, char: 12 },
            end: SourceIndex { line: 0, char: 19 }
        })),
    ))
}

#[test]
fn runtime_error_division_by_zero() {
    assert!(matches!(
        run_code("fn main() { 1 / 0; }", std::io::stdout()),
        Err(RuntimeError::DivisionByZero(Range {
            start: SourceIndex { line: 0, char: 12 },
            end: SourceIndex { line: 0, char: 17 }
        })),
    ))
}

#[test]
fn runtime_error_insert_out_of_range() {
    assert!(matches!(
        run_code(
            "\
fn main() {
    let a = [1, 2, 3];
    a[3];
}
",
            std::io::stdout(),
        ),
        Err(RuntimeError::ArrayIndexOutOfRange {
            size: 3,
            index: 3,
            span: Range {
                start: SourceIndex { line: 2, char: 4 },
                end: SourceIndex { line: 2, char: 8 }
            }
        }),
    ));

    assert!(matches!(
        run_code(
            "\
fn main() {
    let a = [1, 2, 3];
    insert(a, 4, 5);
}
",
            std::io::stdout(),
        ),
        Err(RuntimeError::ArrayIndexOutOfRange {
            size: 3,
            index: 4,
            span: Range {
                start: SourceIndex { line: 2, char: 4 },
                end: SourceIndex { line: 2, char: 19 }
            }
        }),
    ));

    assert!(matches!(
        run_code(
            "\
fn main() {
    let a = [1, 2, 3];
    remove(a, 4);
}
",
            std::io::stdout(),
        ),
        Err(RuntimeError::ArrayIndexOutOfRange {
            size: 3,
            index: 4,
            span: Range {
                start: SourceIndex { line: 2, char: 4 },
                end: SourceIndex { line: 2, char: 16 }
            }
        }),
    ));
}

#[test]
fn runtime_error_write_to_output_failed() {
    assert!(matches!(
        run_code(
            "\
fn main() {
    print(\"asdf\");
}
",
            ErrorTestWriter,
        ),
        Err(RuntimeError::WriteToOutputFailed {
            error: std::io::Error { .. },
            span: Range {
                start: SourceIndex { line: 1, char: 4 },
                end: SourceIndex { line: 1, char: 17 },
            }
        }),
    ));
}

struct ErrorTestWriter;

impl std::io::Write for ErrorTestWriter {
    fn write(&mut self, _: &[u8]) -> std::io::Result<usize> {
        Err(std::io::Error::from(std::io::ErrorKind::Other))
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Err(std::io::Error::from(std::io::ErrorKind::Other))
    }
}
