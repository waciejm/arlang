//! Abstract syntax tree

use std::ops::Range;

use crate::{
    common::{self, Literal, Type},
    source::SourceIndex,
};

use operator::{BinaryOperator, UnaryOperator};

pub mod operator;

/// Abstract syntax tree.
#[derive(Debug, PartialEq)]
pub struct SyntaxTree {
    pub functions: Vec<Function>,
}

#[derive(Debug, PartialEq)]
pub struct Function {
    pub identifier: String,
    pub identifier_span: Range<SourceIndex>,
    pub arguments: Vec<Argument>,
    pub return_type: Option<common::Type>,
    pub signature_span: Range<SourceIndex>,
    pub body: Vec<Statement>,
    pub span: Range<SourceIndex>,
}

#[derive(Debug, PartialEq)]
pub struct Argument {
    pub identifier: String,
    pub r#type: common::Type,
    pub span: Range<SourceIndex>,
}

#[derive(Debug, PartialEq)]
pub enum Statement {
    Declaration {
        identifier: String,
        r#type: Option<common::Type>,
        value: Expression,
        span: Range<SourceIndex>,
    },
    Return {
        value: Expression,
        span: Range<SourceIndex>,
    },
    If {
        branches: Vec<(Expression, Vec<Statement>)>,
        else_branch: Option<Vec<Statement>>,
    },
    While {
        condition: Expression,
        body: Vec<Statement>,
    },
    Expression(Expression),
}

#[derive(Debug, PartialEq)]
pub enum Expression {
    UnaryOp {
        operator: UnaryOperator,
        expression: Box<Expression>,
        span: Range<SourceIndex>,
    },
    BinaryOp {
        operator: BinaryOperator,
        left: Box<Expression>,
        right: Box<Expression>,
        span: Range<SourceIndex>,
    },
    Cast {
        expression: Box<Expression>,
        cast_type: Type,
        span: Range<SourceIndex>,
    },
    Identifier {
        identifier: String,
        span: Range<SourceIndex>,
    },
    ArrayIndex {
        array: Box<Expression>,
        index: Box<Expression>,
        span: Range<SourceIndex>,
    },
    Call {
        identifier: String,
        arguments: Vec<Expression>,
        span: Range<SourceIndex>,
    },
    Literal {
        literal: Literal,
        span: Range<SourceIndex>,
    },
    ArrayInit {
        init: Box<ArrayInit>,
        span: Range<SourceIndex>,
    },
}

impl Expression {
    pub fn span(&self) -> &Range<SourceIndex> {
        match self {
            Expression::UnaryOp { span, .. } => span,
            Expression::BinaryOp { span, .. } => span,
            Expression::Cast { span, .. } => span,
            Expression::Identifier { span, .. } => span,
            Expression::ArrayIndex { span, .. } => span,
            Expression::Call { span, .. } => span,
            Expression::Literal { span, .. } => span,
            Expression::ArrayInit { span, .. } => span,
        }
    }
}

impl From<crate::tokenizer::token::Literal> for Literal {
    fn from(literal: crate::tokenizer::token::Literal) -> Self {
        match literal {
            crate::tokenizer::token::Literal::IntL(int) => Literal::Int(int),
            crate::tokenizer::token::Literal::FloatL(float) => Literal::Float(float),
            crate::tokenizer::token::Literal::BoolL(bool) => Literal::Bool(bool),
            crate::tokenizer::token::Literal::StringL(string) => Literal::String(string),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ArrayInit {
    List(Vec<Expression>),
    Repeat {
        value: Expression,
        repeat: Expression,
    },
}
