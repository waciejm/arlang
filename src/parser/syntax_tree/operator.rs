use std::fmt::Display;

use crate::common::Type;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum UnaryOperator {
    /// `!`
    Not,
    /// `-`
    Minus,
}

impl UnaryOperator {
    pub fn is_type_accepted(&self, arg: &Type) -> bool {
        match self {
            UnaryOperator::Not => matches!(arg, Type::Bool),
            UnaryOperator::Minus => matches!(arg, Type::Int | Type::Float),
        }
    }

    pub fn result_type(&self, arg: &Type) -> Type {
        *arg
    }

    pub fn accepted_types(&self) -> &'static str {
        match self {
            UnaryOperator::Not => "Bool",
            UnaryOperator::Minus => "one of Int, Float",
        }
    }

    fn pretty(&self) -> &'static str {
        match self {
            UnaryOperator::Not => "!",
            UnaryOperator::Minus => "-",
        }
    }
}

impl Display for UnaryOperator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.pretty())
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum BinaryOperator {
    /// `+`
    Plus,
    /// `-`
    Minus,
    /// `*`
    Multiply,
    /// `/`
    Divide,
    /// `<`
    Less,
    /// `<=`
    LessEqual,
    /// `>`
    More,
    /// `>=`
    MoreEqual,
    /// `==`
    Equal,
    /// `!=`
    NotEqual,
    /// `=`
    Assignment,
    /// `&&`
    And,
    /// `||`
    Or,
}

impl BinaryOperator {
    pub fn are_types_accepted(&self, a: &Type, b: &Type) -> bool {
        match self {
            BinaryOperator::Plus => {
                a == b
                    && (matches!(a, Type::Int | Type::Float | Type::String)
                        || a.array_inner().is_some())
            }
            BinaryOperator::Minus => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::Multiply => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::Divide => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::Less => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::LessEqual => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::More => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::MoreEqual => {
                matches!((a, b), (Type::Int, Type::Int) | (Type::Float, Type::Float))
            }
            BinaryOperator::Equal => {
                a == b
                    && (matches!(a, Type::Int | Type::Float | Type::String | Type::Bool)
                        || a.array_inner().is_some())
            }
            BinaryOperator::NotEqual => {
                a == b
                    && (matches!(a, Type::Int | Type::Float | Type::String | Type::Bool)
                        || a.array_inner().is_some())
            }
            BinaryOperator::Assignment => a == b,
            BinaryOperator::And => matches!((a, b), (Type::Bool, Type::Bool)),
            BinaryOperator::Or => matches!((a, b), (Type::Bool, Type::Bool)),
        }
    }

    pub fn result_type(&self, a: &Type, _b: &Type) -> Type {
        match self {
            BinaryOperator::Plus => *a,
            BinaryOperator::Minus => *a,
            BinaryOperator::Multiply => *a,
            BinaryOperator::Divide => *a,
            BinaryOperator::Less => Type::Bool,
            BinaryOperator::LessEqual => Type::Bool,
            BinaryOperator::More => Type::Bool,
            BinaryOperator::MoreEqual => Type::Bool,
            BinaryOperator::Equal => Type::Bool,
            BinaryOperator::NotEqual => Type::Bool,
            BinaryOperator::Assignment => Type::Unit,
            BinaryOperator::And => Type::Bool,
            BinaryOperator::Or => Type::Bool,
        }
    }

    pub fn accepted_types(&self) -> &'static str {
        match self {
            BinaryOperator::Plus => "one of Int + Int, Float + Float, String + String, [T] + [T]",
            BinaryOperator::Minus => "one of Int - Int, Float - Float",
            BinaryOperator::Multiply => "one of Int * Int, Float * Float",
            BinaryOperator::Divide => "one of Int / Int, Float / Float",
            BinaryOperator::Less => "one of Int < Int, Float < Float",
            BinaryOperator::LessEqual => "one of Int <= Int, Float <= Float",
            BinaryOperator::More => "one of Int > Int, Float > Float",
            BinaryOperator::MoreEqual => "one of Int >= Int, Float >= Float",
            BinaryOperator::Equal => {
                "one of Int == Int, Float == Float, String == String, Bool == Bool, [T] == [T]"
            }
            BinaryOperator::NotEqual => {
                "one of Int != Int, Float != Float, String != String, Bool != Bool, [T] != [T]"
            }
            BinaryOperator::Assignment => "one of T = T, [T] = [T]",
            BinaryOperator::And => "Bool && Bool",
            BinaryOperator::Or => "Bool || Bool",
        }
    }

    fn pretty(&self) -> &'static str {
        match self {
            BinaryOperator::Plus => "+",
            BinaryOperator::Minus => "-",
            BinaryOperator::Multiply => "*",
            BinaryOperator::Divide => "/",
            BinaryOperator::Less => "<",
            BinaryOperator::LessEqual => "<=",
            BinaryOperator::More => ">",
            BinaryOperator::MoreEqual => ">=",
            BinaryOperator::Equal => "==",
            BinaryOperator::NotEqual => "!=",
            BinaryOperator::Assignment => "=",
            BinaryOperator::And => "&&",
            BinaryOperator::Or => "||",
        }
    }
}

impl Display for BinaryOperator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.pretty())
    }
}
