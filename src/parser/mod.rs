//! Parser

use std::{io::Read, iter::Peekable, ops::Range};

use crate::{
    common,
    source::{SourceError, SourceIndex},
    tokenizer::{
        token::{prelude::*, Keyword, Literal, Operator, Symbol, Type},
        Tokenizer, TokenizerError, TokenizerItem,
    },
};

use self::syntax_tree::{
    operator::{BinaryOperator, UnaryOperator},
    Argument, ArrayInit, Expression, Function, Statement, SyntaxTree,
};

pub mod syntax_tree;

macro_rules! packed_token {
    ($pattern:pat) => {
        Some(Ok(TokenizerItem {
            token: $pattern,
            span: _,
        }))
    };
}

/// Parser.
/// Builds a [syntax tree](syntax_tree::SyntaxTree) from tokens from [tokenizer](crate::tokenizer::Tokenizer).
#[derive(Debug)]
pub struct Parser<S: Read> {
    tokenizer: Peekable<Tokenizer<S>>,
    last_end: SourceIndex,
}

impl<S: Read> Parser<S> {
    pub fn new(tokenizer: Tokenizer<S>) -> Self {
        Self {
            tokenizer: tokenizer.peekable(),
            last_end: SourceIndex { line: 0, char: 0 },
        }
    }

    pub fn parse(mut self) -> Result<SyntaxTree, ParserError> {
        let mut functions = Vec::new();
        while self.tokenizer.peek().is_some() {
            functions.push(self.parse_function()?);
        }
        Ok(SyntaxTree { functions })
    }

    fn parse_function(&mut self) -> Result<Function, ParserError> {
        let span_start = self.expect_keyword(Fn)?;
        let (identifier, identifier_span) = self.expect_identifier()?;
        self.expect_symbol(LParen)?;
        let arguments = self.parse_arguments()?;
        let argument_span_end = self.expect_symbol(RParen)?.end;
        let return_type_pack = self.parse_function_return_type()?;
        self.expect_symbol(LBrace)?;
        let body = self.parse_body()?;
        let span_end = self.expect_symbol(RBrace)?;

        let signature_span = identifier_span.start
            ..return_type_pack
                .map(|(_, s)| s)
                .unwrap_or(argument_span_end);

        Ok(Function {
            identifier,
            identifier_span,
            arguments,
            return_type: return_type_pack.map(|(rt, _)| rt),
            signature_span,
            body,
            span: span_start.start..span_end.end,
        })
    }

    fn parse_arguments(&mut self) -> Result<Vec<Argument>, ParserError> {
        let mut arguments = Vec::new();
        while let packed_token!(Token::Identifier(_)) = self.tokenizer.peek() {
            let (identifier, span_start) = self.expect_identifier()?;
            self.expect_symbol(Colon)?;
            let (r#type, span_end) = self.parse_type()?;
            if let packed_token!(Token::Symbol(Comma)) = self.tokenizer.peek() {
                self.expect_symbol(Comma)?;
            }
            arguments.push(Argument {
                identifier,
                r#type,
                span: span_start.start..span_end.end,
            })
        }
        Ok(arguments)
    }

    fn parse_function_return_type(
        &mut self,
    ) -> Result<Option<(common::Type, SourceIndex)>, ParserError> {
        match self.tokenizer.peek() {
            packed_token!(Token::Symbol(Colon)) => {
                self.expect_symbol(Colon)?;
                let (r#type, return_type_span) = self.parse_type()?;
                Ok(Some((r#type, return_type_span.end)))
            }
            _ => Ok(None),
        }
    }

    fn parse_type(&mut self) -> Result<(common::Type, Range<SourceIndex>), ParserError> {
        match self.tokenizer.peek() {
            packed_token!(Token::Symbol(LBracket)) => {
                let span_start = self.expect_symbol(LBracket)?;
                let (primitive_type, _) = self.expect_type()?;
                let r#type = match primitive_type {
                    UnitT => common::Type::UnitArray,
                    IntT => common::Type::IntArray,
                    FloatT => common::Type::FloatArray,
                    BoolT => common::Type::BoolArray,
                    StringT => common::Type::StringArray,
                };
                let span_end = self.expect_symbol(RBracket)?;
                Ok((r#type, span_start.start..span_end.end))
            }
            _ => {
                let (primitive_type, range) = self.expect_type()?;
                let r#type = match primitive_type {
                    UnitT => common::Type::Unit,
                    IntT => common::Type::Int,
                    FloatT => common::Type::Float,
                    BoolT => common::Type::Bool,
                    StringT => common::Type::String,
                };
                Ok((r#type, range))
            }
        }
    }

    fn parse_body(&mut self) -> Result<Vec<Statement>, ParserError> {
        let mut statements = Vec::new();
        loop {
            match self.tokenizer.peek() {
                packed_token!(Token::Symbol(RBrace)) => break,
                _ => statements.push(self.parse_statement()?),
            }
        }
        Ok(statements)
    }

    fn parse_statement(&mut self) -> Result<Statement, ParserError> {
        match self.tokenizer.peek() {
            packed_token!(Token::Keyword(Let)) => self.parse_declaration(),
            packed_token!(Token::Keyword(Return)) => self.parse_return(),
            packed_token!(Token::Keyword(If)) => self.parse_if(),
            packed_token!(Token::Keyword(While)) => self.parse_while(),
            _ => self.parse_expression_statement(),
        }
    }

    fn parse_declaration(&mut self) -> Result<Statement, ParserError> {
        let span_start = self.expect_keyword(Let)?;
        let (identifier, _) = self.expect_identifier()?;
        let r#type = if let packed_token!(Token::Symbol(Colon)) = self.tokenizer.peek() {
            self.expect_symbol(Colon)?;
            let (r#type, _) = self.parse_type()?;
            Some(r#type)
        } else {
            None
        };
        self.expect_operator(Assignment)?;
        let expression = self.parse_expression()?;
        let span_end = self.expect_symbol(Semicolon)?;
        Ok(Statement::Declaration {
            identifier,
            r#type,
            value: expression,
            span: span_start.start..span_end.end,
        })
    }

    fn parse_return(&mut self) -> Result<Statement, ParserError> {
        let span_start = self.expect_keyword(Return)?;
        let value = self.parse_expression()?;
        let span_end = self.expect_symbol(Semicolon)?;
        Ok(Statement::Return {
            value,
            span: span_start.start..span_end.end,
        })
    }

    fn parse_if(&mut self) -> Result<Statement, ParserError> {
        let mut branches = Vec::new();
        self.expect_keyword(If)?;
        let if_condition = self.parse_expression()?;
        self.expect_symbol(LBrace)?;
        let if_body = self.parse_body()?;
        self.expect_symbol(RBrace)?;
        branches.push((if_condition, if_body));
        while let packed_token!(Token::Keyword(Elif)) = self.tokenizer.peek() {
            self.expect_keyword(Elif)?;
            let elif_condition = self.parse_expression()?;
            self.expect_symbol(LBrace)?;
            let elif_body = self.parse_body()?;
            self.expect_symbol(RBrace)?;
            branches.push((elif_condition, elif_body));
        }
        let else_branch = if let packed_token!(Token::Keyword(Else)) = self.tokenizer.peek() {
            self.expect_keyword(Else)?;
            self.expect_symbol(LBrace)?;
            let else_body = self.parse_body()?;
            self.expect_symbol(RBrace)?;
            Some(else_body)
        } else {
            None
        };
        Ok(Statement::If {
            branches,
            else_branch,
        })
    }

    fn parse_while(&mut self) -> Result<Statement, ParserError> {
        self.expect_keyword(While)?;
        let condition = self.parse_expression()?;
        self.expect_symbol(LBrace)?;
        let body = self.parse_body()?;
        self.expect_symbol(RBrace)?;
        Ok(Statement::While { condition, body })
    }

    fn parse_expression_statement(&mut self) -> Result<Statement, ParserError> {
        let expression = self.parse_expression()?;
        self.expect_symbol(Semicolon)?;
        Ok(Statement::Expression(expression))
    }

    fn parse_expression(&mut self) -> Result<Expression, ParserError> {
        self.parse_expression_level(ExpressionLevel::top())
    }

    fn parse_expression_level(
        &mut self,
        level: ExpressionLevel,
    ) -> Result<Expression, ParserError> {
        match level {
            level if level.is_binary_op() => self.parse_expression_level_binary_op(level),
            ExpressionLevel::AsCast => self.parse_expression_level_as_cast(),
            ExpressionLevel::UnaryOpNot => self.parse_expression_level_unary_op(),
            ExpressionLevel::ArrayIndex => self.parse_expression_level_array_index(),
            ExpressionLevel::Leaf => self.parse_expression_level_leaf(),
            _ => unreachable!(),
        }
    }

    fn parse_expression_level_binary_op(
        &mut self,
        level: ExpressionLevel,
    ) -> Result<Expression, ParserError> {
        let mut expression = self.parse_expression_level(level.next())?;
        loop {
            match self.tokenizer.peek() {
                packed_token!(Token::Operator(o)) if ExpressionLevel::from(o) == level => {
                    let operator = self.expect_binary_operator()?;
                    let next = self.parse_expression_level(level.next())?;
                    let span = expression.span().start..next.span().end;
                    expression = Expression::BinaryOp {
                        operator,
                        left: expression.into(),
                        right: next.into(),
                        span,
                    }
                }
                _ => return Ok(expression),
            }
        }
    }

    fn parse_expression_level_as_cast(&mut self) -> Result<Expression, ParserError> {
        let mut expression = self.parse_expression_level(ExpressionLevel::AsCast.next())?;
        while let packed_token!(Token::Operator(As)) = self.tokenizer.peek() {
            self.expect_operator(As)?;
            let (cast_type, span_end) = self.parse_type()?;
            let span = expression.span().start..span_end.end;
            expression = Expression::Cast {
                expression: expression.into(),
                cast_type,
                span,
            };
        }
        Ok(expression)
    }

    fn parse_expression_level_unary_op(&mut self) -> Result<Expression, ParserError> {
        match self.check_for_unary_operator() {
            Some((operator, span_start)) => {
                let expression = self.parse_expression_level(ExpressionLevel::UnaryOpNot)?;
                let span_end = expression.span().clone().end;
                Ok(Expression::UnaryOp {
                    operator,
                    expression: expression.into(),
                    span: span_start..span_end,
                })
            }
            None => self.parse_expression_level(ExpressionLevel::UnaryOpNot.next()),
        }
    }

    fn parse_expression_level_array_index(&mut self) -> Result<Expression, ParserError> {
        let expression = self.parse_expression_level(ExpressionLevel::ArrayIndex.next())?;
        match self.tokenizer.peek() {
            packed_token!(Token::Symbol(LBracket)) => {
                self.expect_symbol(LBracket)?;
                let inner = self.parse_expression()?;
                let span_start = expression.span().clone().start;
                let span_end = self.expect_symbol(RBracket)?;
                Ok(Expression::ArrayIndex {
                    array: expression.into(),
                    index: inner.into(),
                    span: span_start..span_end.end,
                })
            }
            _ => Ok(expression),
        }
    }

    fn parse_expression_level_leaf(&mut self) -> Result<Expression, ParserError> {
        match self.tokenizer.peek() {
            packed_token!(Token::Identifier(_)) => {
                let (identifier, span_start) = self.expect_identifier()?;
                match self.tokenizer.peek() {
                    packed_token!(Token::Symbol(LParen)) => {
                        self.parse_call(identifier, span_start.start)
                    }
                    _ => Ok(Expression::Identifier {
                        identifier,
                        span: span_start,
                    }),
                }
            }
            packed_token!(Token::Literal(_)) => {
                let (literal, span) = self.expect_literal()?;
                Ok(Expression::Literal {
                    literal: literal.into(),
                    span,
                })
            }
            packed_token!(Token::Symbol(LBracket)) => self.parse_array_init(),
            packed_token!(Token::Symbol(LParen)) => {
                let span_start = self.expect_symbol(LParen)?;
                match self.tokenizer.peek() {
                    packed_token!(Token::Symbol(RParen)) => {
                        let span_end = self.expect_symbol(RParen)?;
                        Ok(Expression::Literal {
                            literal: common::Literal::Unit,
                            span: span_start.start..span_end.end,
                        })
                    }
                    _ => {
                        let expression = self.parse_expression()?;
                        self.expect_symbol(RParen)?;
                        Ok(expression)
                    }
                }
            }
            _ => {
                let next = self.expect_next("expression")?;
                Err(ParserError::UnexpectedToken {
                    expected: "expression",
                    found: next.token,
                    span: next.span,
                })
            }
        }
    }

    fn parse_call(
        &mut self,
        identifier: String,
        span_start: SourceIndex,
    ) -> Result<Expression, ParserError> {
        self.expect_symbol(LParen)?;
        let mut arguments = Vec::new();
        loop {
            match self.tokenizer.peek() {
                packed_token!(Token::Symbol(RParen)) => break,
                _ => {
                    arguments.push(self.parse_expression()?);
                    if let packed_token!(Token::Symbol(Comma)) = self.tokenizer.peek() {
                        self.expect_symbol(Comma)?;
                    }
                }
            }
        }
        let span_end = self.expect_symbol(RParen)?;
        Ok(Expression::Call {
            identifier,
            arguments,
            span: span_start..span_end.end,
        })
    }

    fn parse_array_init(&mut self) -> Result<Expression, ParserError> {
        let span_start = self.expect_symbol(LBracket)?;
        match self.tokenizer.peek() {
            packed_token!(Token::Symbol(RBracket)) => {
                let span_end = self.expect_symbol(RBracket)?;
                Ok(Expression::ArrayInit {
                    init: ArrayInit::List(vec![]).into(),
                    span: span_start.start..span_end.end,
                })
            }
            _ => self.continue_parse_non_empty_array_init(span_start.start),
        }
    }

    fn continue_parse_non_empty_array_init(
        &mut self,
        span_start: SourceIndex,
    ) -> Result<Expression, ParserError> {
        let first = self.parse_expression()?;
        match self.tokenizer.peek() {
            packed_token!(Token::Symbol(Comma)) => {
                self.continue_parse_array_init_repeat(span_start, first)
            }
            packed_token!(Token::Symbol(RBracket)) => {
                self.continue_parse_array_init_list_single_element(span_start, first)
            }
            packed_token!(Token::Symbol(Semicolon)) => {
                self.continue_parse_array_init_list_multiple_elements(span_start, first)
            }
            _ => {
                let TokenizerItem { token, span: range } =
                    self.expect_next("symbols `]`, `,` or `;`")?;
                Err(ParserError::UnexpectedToken {
                    expected: "symbols `]`, `,` or `;`",
                    found: token,
                    span: range,
                })
            }
        }
    }

    fn continue_parse_array_init_list_single_element(
        &mut self,
        span_start: SourceIndex,
        first: Expression,
    ) -> Result<Expression, ParserError> {
        let span_end = self.expect_symbol(RBracket)?;
        Ok(Expression::ArrayInit {
            init: ArrayInit::List(vec![first]).into(),
            span: span_start..span_end.end,
        })
    }

    fn continue_parse_array_init_list_multiple_elements(
        &mut self,
        span_start: SourceIndex,
        first: Expression,
    ) -> Result<Expression, ParserError> {
        self.expect_symbol(Semicolon)?;
        let repeat = self.parse_expression()?;
        let span_end = self.expect_symbol(RBracket)?;
        Ok(Expression::ArrayInit {
            init: ArrayInit::Repeat {
                value: first,
                repeat,
            }
            .into(),
            span: span_start..span_end.end,
        })
    }

    fn continue_parse_array_init_repeat(
        &mut self,
        span_start: SourceIndex,
        first: Expression,
    ) -> Result<Expression, ParserError> {
        self.expect_symbol(Comma)?;
        let mut expressions = vec![first];
        loop {
            match self.tokenizer.peek() {
                packed_token!(Token::Symbol(RBracket)) => break,
                _ => {
                    expressions.push(self.parse_expression()?);
                    if let packed_token!(Token::Symbol(Comma)) = self.tokenizer.peek() {
                        self.expect_symbol(Comma)?;
                    }
                }
            }
        }
        let span_end = self.expect_symbol(RBracket)?;
        Ok(Expression::ArrayInit {
            init: ArrayInit::List(expressions).into(),
            span: span_start..span_end.end,
        })
    }
}

#[derive(Debug, PartialEq)]
enum ExpressionLevel {
    BinaryOpAssignment,
    BinaryOpOr,
    BinaryOpAnd,
    BinaryOpCompare,
    BinaryOpAddSub,
    BinaryOpMulDiv,
    AsCast,
    UnaryOpNot,
    ArrayIndex,
    Leaf,
}

impl ExpressionLevel {
    fn top() -> Self {
        ExpressionLevel::BinaryOpAssignment
    }

    fn is_binary_op(&self) -> bool {
        use ExpressionLevel::*;
        match self {
            BinaryOpAssignment => true,
            BinaryOpOr => true,
            BinaryOpAnd => true,
            BinaryOpCompare => true,
            BinaryOpAddSub => true,
            BinaryOpMulDiv => true,
            AsCast => false,
            UnaryOpNot => false,
            ArrayIndex => false,
            Leaf => false,
        }
    }

    fn next(&self) -> Self {
        use ExpressionLevel::*;
        match self {
            BinaryOpAssignment => BinaryOpOr,
            BinaryOpOr => BinaryOpAnd,
            BinaryOpAnd => BinaryOpCompare,
            BinaryOpCompare => BinaryOpAddSub,
            BinaryOpAddSub => BinaryOpMulDiv,
            BinaryOpMulDiv => AsCast,
            AsCast => UnaryOpNot,
            UnaryOpNot => ArrayIndex,
            ArrayIndex => Leaf,
            Leaf => panic!("No next level for {:?}", self),
        }
    }
}

impl From<&Operator> for ExpressionLevel {
    fn from(operator: &Operator) -> Self {
        use ExpressionLevel::*;
        match operator {
            Assignment => BinaryOpAssignment,
            Or => BinaryOpOr,
            And => BinaryOpAnd,
            Less => BinaryOpCompare,
            LessEqual => BinaryOpCompare,
            More => BinaryOpCompare,
            MoreEqual => BinaryOpCompare,
            Equal => BinaryOpCompare,
            NotEqual => BinaryOpCompare,
            Plus => BinaryOpAddSub,
            Minus => BinaryOpAddSub,
            Multiply => BinaryOpMulDiv,
            Divide => BinaryOpMulDiv,
            As => AsCast,
            Not => UnaryOpNot,
        }
    }
}

/// Expectations
impl<S: Read> Parser<S> {
    fn expect_next(
        &mut self,
        expected_message: &'static str,
    ) -> Result<TokenizerItem, ParserError> {
        match self.tokenizer.next() {
            Some(t) => {
                if let Ok(ti) = &t {
                    self.last_end = ti.span.end;
                }
                t.map_err(ParserError::TokenizerError)
            }
            _ => Err(ParserError::UnexpectedEndOfTokens {
                expected: expected_message,
                source_end: self.last_end,
            }),
        }
    }

    fn expect_keyword(&mut self, keyword: Keyword) -> Result<Range<SourceIndex>, ParserError> {
        let expected_message = match keyword {
            Fn => "keyword `fn`",
            Let => "keyword `let`",
            If => "keyword `if`",
            Elif => "keyword `elif`",
            Else => "keyword `else`",
            While => "keyword `while`",
            Return => "keyword `return`",
        };
        match self.expect_next(expected_message)? {
            TokenizerItem {
                token: Token::Keyword(k),
                span: range,
            } if k == keyword => Ok(range),
            TokenizerItem { token, span: range } => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: token,
                span: range,
            }),
        }
    }

    fn expect_symbol(&mut self, symbol: Symbol) -> Result<Range<SourceIndex>, ParserError> {
        let expected_message = match symbol {
            Comma => "symbol `,`",
            Colon => "symbol `:`",
            Semicolon => "symbol `;`",
            LParen => "symbol `(`",
            RParen => "symbol `)`",
            LBracket => "symbol `[`",
            RBracket => "symbol `]`",
            LBrace => "symbol `{`",
            RBrace => "symbol `}`",
        };
        match self.expect_next(expected_message)? {
            TokenizerItem {
                token: Token::Symbol(s),
                span: range,
            } if s == symbol => Ok(range),
            TokenizerItem { token, span: range } => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: token,
                span: range,
            }),
        }
    }

    fn expect_operator(&mut self, operator: Operator) -> Result<Range<SourceIndex>, ParserError> {
        let expected_message = match operator {
            Plus => "operator `+`",
            Minus => "operator `-`",
            Multiply => "operator `*`",
            Divide => "operator `/`",
            Less => "operator `<`",
            LessEqual => "operator `<=`",
            More => "operator `>`",
            MoreEqual => "operator `>=`",
            Assignment => "operator `=`",
            Equal => "operator `==`",
            NotEqual => "operator `!=`",
            And => "operator `&&`",
            Or => "operator `||`",
            Not => "operator `!`",
            As => "operator `as`",
        };
        match self.expect_next(expected_message)? {
            TokenizerItem {
                token: Token::Operator(o),
                span: range,
            } if o == operator => Ok(range),
            TokenizerItem { token, span: range } => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: token,
                span: range,
            }),
        }
    }

    fn expect_binary_operator(&mut self) -> Result<BinaryOperator, ParserError> {
        let expected_message = "binary operator";
        let next = self.expect_next(expected_message)?;
        match &next.token {
            Token::Operator(Plus) => Ok(BinaryOperator::Plus),
            Token::Operator(Minus) => Ok(BinaryOperator::Minus),
            Token::Operator(Multiply) => Ok(BinaryOperator::Multiply),
            Token::Operator(Divide) => Ok(BinaryOperator::Divide),
            Token::Operator(Less) => Ok(BinaryOperator::Less),
            Token::Operator(LessEqual) => Ok(BinaryOperator::LessEqual),
            Token::Operator(More) => Ok(BinaryOperator::More),
            Token::Operator(MoreEqual) => Ok(BinaryOperator::MoreEqual),
            Token::Operator(Assignment) => Ok(BinaryOperator::Assignment),
            Token::Operator(Equal) => Ok(BinaryOperator::Equal),
            Token::Operator(NotEqual) => Ok(BinaryOperator::NotEqual),
            Token::Operator(And) => Ok(BinaryOperator::And),
            Token::Operator(Or) => Ok(BinaryOperator::Or),
            _ => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: next.token,
                span: next.span,
            }),
        }
    }

    fn expect_identifier(&mut self) -> Result<(String, Range<SourceIndex>), ParserError> {
        let expected_message = "identifier";
        match self.expect_next(expected_message)? {
            TokenizerItem {
                token: Token::Identifier(identifier),
                span: range,
            } => Ok((identifier, range)),
            TokenizerItem { token, span: range } => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: token,
                span: range,
            }),
        }
    }

    fn expect_type(&mut self) -> Result<(Type, Range<SourceIndex>), ParserError> {
        let expected_message = "type";
        match self.expect_next(expected_message)? {
            TokenizerItem {
                token: Token::Type(t),
                span: range,
            } => Ok((t, range)),
            TokenizerItem { token, span: range } => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: token,
                span: range,
            }),
        }
    }

    fn expect_literal(&mut self) -> Result<(Literal, Range<SourceIndex>), ParserError> {
        let expected_message = "literal";
        match self.expect_next(expected_message)? {
            TokenizerItem {
                token: Token::Literal(l),
                span: range,
            } => Ok((l, range)),
            TokenizerItem { token, span: range } => Err(ParserError::UnexpectedToken {
                expected: expected_message,
                found: token,
                span: range,
            }),
        }
    }

    fn check_for_unary_operator(&mut self) -> Option<(UnaryOperator, SourceIndex)> {
        match self.tokenizer.peek() {
            packed_token!(Token::Operator(Minus)) => Some((
                UnaryOperator::Minus,
                self.expect_operator(Minus).unwrap().start,
            )),
            packed_token!(Token::Operator(Not)) => {
                Some((UnaryOperator::Not, self.expect_operator(Not).unwrap().start))
            }
            _ => None,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ParserError {
    TokenizerError(TokenizerError),
    UnexpectedEndOfTokens {
        expected: &'static str,
        source_end: SourceIndex,
    },
    UnexpectedToken {
        expected: &'static str,
        found: Token,
        span: Range<SourceIndex>,
    },
}

impl From<TokenizerError> for ParserError {
    fn from(error: TokenizerError) -> Self {
        Self::TokenizerError(error)
    }
}

impl From<ParserError> for SourceError {
    fn from(error: ParserError) -> Self {
        match error {
            ParserError::TokenizerError(e) => e.into(),
            ParserError::UnexpectedEndOfTokens {
                expected,
                source_end,
            } => SourceError::new(
                source_end.span_one(),
                format!("PARSER ERROR: Expected {} but token stream ended", expected),
            ),
            ParserError::UnexpectedToken {
                expected,
                found,
                span,
            } => SourceError::new(
                span,
                format!(
                    "PARSER ERROR: Expected {}, found {} instead",
                    expected, found,
                ),
            ),
        }
    }
}

#[cfg(test)]
mod test;
