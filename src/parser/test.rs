use std::ops::Range;

use crate::{
    common::{Literal, Type},
    parser::{
        syntax_tree::{
            operator::{BinaryOperator, UnaryOperator},
            Argument, ArrayInit, Expression, Function, Statement,
        },
        Parser, ParserError,
    },
    source::{SourceIndex, SourceIter},
    tokenizer::{
        token::{self, Keyword, Operator, Symbol, Token},
        Tokenizer, TokenizerError,
    },
};

#[test]
fn example_program_parses() {
    assert!(make_parser(include_str!("../../example.ar"))
        .parse()
        .is_ok());
}

#[test]
fn syntax_tree() {
    assert!(
        make_parser(
            "\
fn main() {
    let a = hello_world(3);
}

fn print_hello_world(times: Int): Int {
    let i = 0;
    while i < times {
        i = i + 1;
        print(\"hello world\");
    }
    return times;
}
        "
        )
        .parse()
        .unwrap()
        .functions
        .len()
            == 2
    );
}

#[test]
fn function() {
    assert_eq!(
        make_parser("fn a() {}").parse_function().unwrap(),
        Function {
            identifier: "a".into(),
            identifier_span: SourceIndex { line: 0, char: 3 }..SourceIndex { line: 0, char: 4 },
            arguments: Vec::new(),
            return_type: None,
            signature_span: SourceIndex { line: 0, char: 3 }..SourceIndex { line: 0, char: 6 },
            body: Vec::new(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 9 },
        },
    );
    assert_eq!(
        make_parser("fn a(a: Int, b: Float,): [Bool] { (); }")
            .parse_function()
            .unwrap(),
        Function {
            identifier: "a".into(),
            identifier_span: SourceIndex { line: 0, char: 3 }..SourceIndex { line: 0, char: 4 },
            arguments: vec![
                Argument {
                    identifier: "a".into(),
                    r#type: Type::Int,
                    span: SourceIndex { line: 0, char: 5 }..SourceIndex { line: 0, char: 11 },
                },
                Argument {
                    identifier: "b".into(),
                    r#type: Type::Float,
                    span: SourceIndex { line: 0, char: 13 }..SourceIndex { line: 0, char: 21 },
                },
            ],
            return_type: Some(Type::BoolArray),
            signature_span: SourceIndex { line: 0, char: 3 }..SourceIndex { line: 0, char: 31 },
            body: vec![Statement::Expression(expression_unit_at_index(34))],
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 39 },
        },
    );
}

#[test]
fn arguments() {
    assert_eq!(
        make_parser("a: Unit, b: [Int],").parse_arguments().unwrap(),
        vec![
            Argument {
                identifier: "a".into(),
                r#type: Type::Unit,
                span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 7 },
            },
            Argument {
                identifier: "b".into(),
                r#type: Type::IntArray,
                span: SourceIndex { line: 0, char: 9 }..SourceIndex { line: 0, char: 17 },
            },
        ],
    );
}

#[test]
fn statement_declaration() {
    assert_eq!(
        make_parser("let a = ();").parse_statement().unwrap(),
        Statement::Declaration {
            identifier: "a".into(),
            r#type: None,
            value: expression_unit_at_index(8),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 11 },
        }
    );
    assert_eq!(
        make_parser("let a: Unit = ();").parse_statement().unwrap(),
        Statement::Declaration {
            identifier: "a".into(),
            r#type: Some(Type::Unit),
            value: expression_unit_at_index(14),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 17 },
        }
    );
}

#[test]
fn statement_return() {
    assert_eq!(
        make_parser("return ();").parse_statement().unwrap(),
        Statement::Return {
            value: expression_unit_at_index(7),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 10 },
        }
    )
}

#[test]
fn statement_if() {
    assert_eq!(
        make_parser("if () {();} elif () {();} elif () {();} else {();}")
            .parse_statement()
            .unwrap(),
        Statement::If {
            branches: vec![
                (
                    expression_unit_at_index(3),
                    vec![Statement::Expression(expression_unit_at_index(7))]
                ),
                (
                    expression_unit_at_index(17),
                    vec![Statement::Expression(expression_unit_at_index(21))],
                ),
                (
                    expression_unit_at_index(31),
                    vec![Statement::Expression(expression_unit_at_index(35))],
                ),
            ],
            else_branch: Some(vec![Statement::Expression(expression_unit_at_index(46))]),
        }
    )
}

#[test]
fn statement_while() {
    assert!(matches!(
        make_parser("while true { fun(); }")
            .parse_statement()
            .unwrap(),
        Statement::While {
            condition: Expression::Literal {
                literal: Literal::Bool(true),
                span: Range {
                    start: SourceIndex { line: 0, char: 6 },
                    end: SourceIndex { line: 0, char: 10 },
                },
            },
            body: _,
        },
    ))
}

#[test]
fn statement_expression() {
    assert_eq!(
        make_parser("1 + 2;").parse_statement().unwrap(),
        Statement::Expression(Expression::BinaryOp {
            operator: BinaryOperator::Plus,
            left: Box::new(Expression::Literal {
                literal: Literal::Int(1),
                span: Range {
                    start: SourceIndex { line: 0, char: 0 },
                    end: SourceIndex { line: 0, char: 1 },
                },
            }),
            right: Box::new(Expression::Literal {
                literal: Literal::Int(2),
                span: Range {
                    start: SourceIndex { line: 0, char: 4 },
                    end: SourceIndex { line: 0, char: 5 },
                },
            }),
            span: Range {
                start: SourceIndex { line: 0, char: 0 },
                end: SourceIndex { line: 0, char: 5 },
            },
        })
    )
}

#[test]
fn complex_expression() {
    assert_eq!(
        make_parser(
            "\
-!()[()] as Unit * () / () + () - () < () && () || () = ()
        "
        )
        .parse_expression()
        .unwrap(),
        Expression::BinaryOp {
            operator: BinaryOperator::Assignment,
            left: Box::new(Expression::BinaryOp {
                operator: BinaryOperator::Or,
                left: Box::new(Expression::BinaryOp {
                    operator: BinaryOperator::And,
                    left: Box::new(Expression::BinaryOp {
                        operator: BinaryOperator::Less,
                        left: Box::new(Expression::BinaryOp {
                            operator: BinaryOperator::Minus,
                            left: Box::new(Expression::BinaryOp {
                                operator: BinaryOperator::Plus,
                                left: Box::new(Expression::BinaryOp {
                                    operator: BinaryOperator::Divide,
                                    left: Box::new(Expression::BinaryOp {
                                        operator: BinaryOperator::Multiply,
                                        left: Box::new(Expression::Cast {
                                            expression: Box::new(Expression::UnaryOp {
                                                operator: UnaryOperator::Minus,
                                                expression: Box::new(Expression::UnaryOp {
                                                    operator: UnaryOperator::Not,
                                                    expression: Box::new(Expression::ArrayIndex {
                                                        array: expression_unit_at_index(2).into(),
                                                        index: expression_unit_at_index(5).into(),
                                                        span: SourceIndex { line: 0, char: 2 }
                                                            ..SourceIndex { line: 0, char: 8 },
                                                    }),
                                                    span: SourceIndex { line: 0, char: 1 }
                                                        ..SourceIndex { line: 0, char: 8 },
                                                }),
                                                span: SourceIndex { line: 0, char: 0 }
                                                    ..SourceIndex { line: 0, char: 8 },
                                            }),
                                            cast_type: crate::common::Type::Unit,
                                            span: SourceIndex { line: 0, char: 0 }..SourceIndex {
                                                line: 0,
                                                char: 16
                                            },
                                        }),
                                        right: expression_unit_at_index(19).into(),
                                        span: SourceIndex { line: 0, char: 0 }..SourceIndex {
                                            line: 0,
                                            char: 21
                                        },
                                    }),
                                    right: expression_unit_at_index(24).into(),
                                    span: SourceIndex { line: 0, char: 0 }..SourceIndex {
                                        line: 0,
                                        char: 26
                                    },
                                }),
                                right: expression_unit_at_index(29).into(),
                                span: SourceIndex { line: 0, char: 0 }..SourceIndex {
                                    line: 0,
                                    char: 31
                                },
                            }),
                            right: expression_unit_at_index(34).into(),
                            span: SourceIndex { line: 0, char: 0 }..SourceIndex {
                                line: 0,
                                char: 36
                            },
                        }),
                        right: expression_unit_at_index(39).into(),
                        span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 41 },
                    }),
                    right: expression_unit_at_index(45).into(),
                    span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 47 },
                }),
                right: expression_unit_at_index(51).into(),
                span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 53 },
            }),
            right: expression_unit_at_index(56).into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 58 },
        },
    )
}

#[test]
fn expression_identifier() {
    assert_eq!(
        make_parser("asd_123").parse_expression().unwrap(),
        Expression::Identifier {
            identifier: "asd_123".into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 7 },
        },
    )
}

#[test]
fn expression_array_read() {
    assert_eq!(
        make_parser("()[()]").parse_expression().unwrap(),
        Expression::ArrayIndex {
            array: expression_unit_at_index(0).into(),
            index: expression_unit_at_index(3).into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 6 },
        },
    );
}

#[test]
fn expression_call() {
    assert_eq!(
        make_parser("function()").parse_expression().unwrap(),
        Expression::Call {
            identifier: "function".into(),
            arguments: Vec::new(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 10 },
        },
    );
    assert_eq!(
        make_parser("function((),(),)").parse_expression().unwrap(),
        Expression::Call {
            identifier: "function".into(),
            arguments: vec![expression_unit_at_index(9), expression_unit_at_index(12)],
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 16 },
        },
    );
}

#[test]
fn expression_literal() {
    assert_eq!(
        make_parser("()").parse_expression().unwrap(),
        Expression::Literal {
            literal: Literal::Unit,
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 2 },
        },
    );
    assert_eq!(
        make_parser("1234").parse_expression().unwrap(),
        Expression::Literal {
            literal: Literal::Int(1234),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 4 },
        },
    );
    assert_eq!(
        make_parser("1234.0").parse_expression().unwrap(),
        Expression::Literal {
            literal: Literal::Float(1234.0),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 6 },
        },
    );
    assert_eq!(
        make_parser("false").parse_expression().unwrap(),
        Expression::Literal {
            literal: Literal::Bool(false),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 5 },
        },
    );
    assert_eq!(
        make_parser("\"asdf\"").parse_expression().unwrap(),
        Expression::Literal {
            literal: Literal::String("asdf".into()),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 6 },
        },
    );
}

#[test]
fn expression_array_init() {
    assert_eq!(
        make_parser("[]").parse_expression().unwrap(),
        Expression::ArrayInit {
            init: ArrayInit::List(vec![]).into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 2 },
        },
    );
    assert_eq!(
        make_parser("[(), 1, 2.0, true, \"asd\"]")
            .parse_expression()
            .unwrap(),
        Expression::ArrayInit {
            init: ArrayInit::List(vec![
                Expression::Literal {
                    literal: Literal::Unit,
                    span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 3 },
                },
                Expression::Literal {
                    literal: Literal::Int(1),
                    span: SourceIndex { line: 0, char: 5 }..SourceIndex { line: 0, char: 6 },
                },
                Expression::Literal {
                    literal: Literal::Float(2.0),
                    span: SourceIndex { line: 0, char: 8 }..SourceIndex { line: 0, char: 11 },
                },
                Expression::Literal {
                    literal: Literal::Bool(true),
                    span: SourceIndex { line: 0, char: 13 }..SourceIndex { line: 0, char: 17 },
                },
                Expression::Literal {
                    literal: Literal::String("asd".into()),
                    span: SourceIndex { line: 0, char: 19 }..SourceIndex { line: 0, char: 24 },
                },
            ])
            .into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 25 },
        },
    );
    assert_eq!(
        make_parser("[0;0]").parse_expression().unwrap(),
        Expression::ArrayInit {
            init: ArrayInit::Repeat {
                value: Expression::Literal {
                    literal: Literal::Int(0),
                    span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
                },
                repeat: Expression::Literal {
                    literal: Literal::Int(0),
                    span: SourceIndex { line: 0, char: 3 }..SourceIndex { line: 0, char: 4 },
                },
            }
            .into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 5 },
        },
    );
    assert_eq!(
        make_parser("[0;100]").parse_expression().unwrap(),
        Expression::ArrayInit {
            init: ArrayInit::Repeat {
                value: Expression::Literal {
                    literal: Literal::Int(0),
                    span: SourceIndex { line: 0, char: 1 }..SourceIndex { line: 0, char: 2 },
                },
                repeat: Expression::Literal {
                    literal: Literal::Int(100),
                    span: SourceIndex { line: 0, char: 3 }..SourceIndex { line: 0, char: 6 },
                },
            }
            .into(),
            span: SourceIndex { line: 0, char: 0 }..SourceIndex { line: 0, char: 7 },
        },
    );
}

#[test]
fn parse_error_token_error() {
    assert_eq!(
        make_parser("~").parse().unwrap_err(),
        ParserError::TokenizerError(TokenizerError::UnexpectedTokenStart(
            SourceIndex::new(0, 0),
            '~'
        ))
    );
}

#[test]
fn parse_error_end_of_tokens() {
    assert_eq!(
        make_parser("fn main").parse().unwrap_err(),
        ParserError::UnexpectedEndOfTokens {
            expected: "symbol `(`",
            source_end: SourceIndex::new(0, 7)
        },
    );
}

#[test]
fn parse_fail_function() {
    assert_eq!(
        make_parser("main() {}").parse().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "keyword `fn`",
            found: Token::Identifier("main".into()),
            span: SourceIndex::span((0, 0), (0, 4))
        }
    );
    assert_eq!(
        make_parser("fn () {}").parse().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "identifier",
            found: Token::Symbol(Symbol::LParen),
            span: SourceIndex::span((0, 3), (0, 4))
        }
    );
    assert_eq!(
        make_parser("fn main {}").parse().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `(`",
            found: Token::Symbol(Symbol::LBrace),
            span: SourceIndex::span((0, 8), (0, 9))
        }
    );
    assert_eq!(
        make_parser("fn main(): {}").parse().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "type",
            found: Token::Symbol(Symbol::LBrace),
            span: SourceIndex::span((0, 11), (0, 12))
        }
    );
}

#[test]
fn parse_fail_statement_declaration() {
    assert_eq!(
        make_parser("let = 1;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "identifier",
            found: Token::Operator(Operator::Assignment),
            span: SourceIndex::span((0, 4), (0, 5))
        }
    );
    assert_eq!(
        make_parser("let a 1;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "operator `=`",
            found: Token::Literal(token::Literal::IntL(1)),
            span: SourceIndex::span((0, 6), (0, 7))
        }
    );
    assert_eq!(
        make_parser("let a: = 1;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "type",
            found: Token::Operator(Operator::Assignment),
            span: SourceIndex::span((0, 7), (0, 8))
        }
    );
    assert_eq!(
        make_parser("let a;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "operator `=`",
            found: Token::Symbol(Symbol::Semicolon),
            span: SourceIndex::span((0, 5), (0, 6))
        }
    );
    assert_eq!(
        make_parser("let a = ;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "expression",
            found: Token::Symbol(Symbol::Semicolon),
            span: SourceIndex::span((0, 8), (0, 9))
        }
    );
}

#[test]
fn parse_fail_statement_return() {
    assert_eq!(
        make_parser("return;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "expression",
            found: Token::Symbol(Symbol::Semicolon),
            span: SourceIndex::span((0, 6), (0, 7))
        }
    );
    assert_eq!(
        make_parser("return 1 }").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `;`",
            found: Token::Symbol(Symbol::RBrace),
            span: SourceIndex::span((0, 9), (0, 10))
        }
    );
}
#[test]
fn parse_fail_statement_if() {
    assert_eq!(
        make_parser("if true return false;")
            .parse_statement()
            .unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `{`",
            found: Token::Keyword(Keyword::Return),
            span: SourceIndex::span((0, 8), (0, 14))
        },
    );
    assert_eq!(
        make_parser("if {").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "expression",
            found: Token::Symbol(Symbol::LBrace),
            span: SourceIndex::span((0, 3), (0, 4))
        },
    );
    assert_eq!(
        make_parser("if () {} elif {}")
            .parse_statement()
            .unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "expression",
            found: Token::Symbol(Symbol::LBrace),
            span: SourceIndex::span((0, 14), (0, 15))
        },
    );
    assert_eq!(
        make_parser("if () {} else true {}")
            .parse_statement()
            .unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `{`",
            found: Token::Literal(token::Literal::BoolL(true)),
            span: SourceIndex::span((0, 14), (0, 18))
        },
    );
}

#[test]
fn parse_fail_statement_while() {
    assert_eq!(
        make_parser("while {}").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "expression",
            found: Token::Symbol(Symbol::LBrace),
            span: SourceIndex::span((0, 6), (0, 7))
        },
    );
    assert_eq!(
        make_parser("while true return ();")
            .parse_statement()
            .unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `{`",
            found: Token::Keyword(Keyword::Return),
            span: SourceIndex::span((0, 11), (0, 17))
        },
    );
}

#[test]
fn parse_fail_statement_expression() {
    assert_eq!(
        make_parser("1 + 1 let a = 1;")
            .parse_statement()
            .unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `;`",
            found: Token::Keyword(Keyword::Let),
            span: SourceIndex::span((0, 6), (0, 9))
        },
    );
    assert_eq!(
        make_parser("1 1;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "symbol `;`",
            found: Token::Literal(token::Literal::IntL(1)),
            span: SourceIndex::span((0, 2), (0, 3))
        },
    );
    assert_eq!(
        make_parser("+1;").parse_statement().unwrap_err(),
        ParserError::UnexpectedToken {
            expected: "expression",
            found: Token::Operator(Operator::Plus),
            span: SourceIndex::span((0, 0), (0, 1))
        },
    );
}

fn make_parser(src: &str) -> Parser<&[u8]> {
    Parser::new(Tokenizer::new(SourceIter::new(src.as_bytes())))
}

fn expression_unit_at_index(char: usize) -> Expression {
    Expression::Literal {
        literal: Literal::Unit,
        span: SourceIndex { line: 0, char }..SourceIndex {
            line: 0,
            char: char + 2,
        },
    }
}
