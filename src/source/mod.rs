//! Representation of the source code.

use std::{
    error::Error,
    fmt::{Debug, Display},
    io::{Bytes, Read},
    ops::Range,
    rc::Rc,
};

use utf8_decode::UnsafeDecoder;

/// Source code container.
#[derive(Debug, Clone)]
pub struct Source {
    pub lines: Rc<[Rc<str>]>,
}

impl Source {
    pub fn new<T: Read>(mut src: T) -> Result<Self, InputError> {
        let mut bytes = Vec::new();
        src.read_to_end(&mut bytes).map_err(InputError::IOError)?;
        let str = std::str::from_utf8(&bytes).map_err(|_| InputError::NotUTF8)?;
        let lines = str
            .split_inclusive("\n")
            .map(|str| str.to_string().into())
            .collect::<Vec<_>>();
        Ok(Self {
            lines: lines.into(),
        })
    }
}

/// An iterator over the source code.
/// Yields [SourceIterItems](SourceIterItem).
pub struct SourceIter<S: Read> {
    decoder: UnsafeDecoder<Bytes<S>>,
    next: SourceIndex,
}

impl<S: Read> SourceIter<S> {
    pub fn new(source: S) -> Self {
        let decoder = UnsafeDecoder::new(source.bytes());
        Self {
            decoder,
            next: SourceIndex { line: 0, char: 0 },
        }
    }
}

impl<S: Read> Iterator for SourceIter<S> {
    type Item = SourceIterItem;

    fn next(&mut self) -> Option<Self::Item> {
        match self.decoder.next() {
            Some(s) => match s.unwrap() {
                '\n' => {
                    let ret = SourceIterItem {
                        char: '\n',
                        index: self.next,
                    };
                    self.next = SourceIndex::new(self.next.line + 1, 0);
                    Some(ret)
                }
                c => {
                    let ret = SourceIterItem {
                        char: c,
                        index: self.next,
                    };
                    self.next = SourceIndex::new(self.next.line, self.next.char + 1);
                    Some(ret)
                }
            },
            None => None,
        }
    }
}

impl<S: Read> Debug for SourceIter<S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SourceIter")
            .field("next", &self.next)
            .finish()
    }
}

/// Item yielded by the [SourceIter].
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SourceIterItem {
    pub char: char,
    pub index: SourceIndex,
}

/// Source code input error.
#[derive(Debug)]
pub enum InputError {
    /// [IO error](std::io::Error) encountered when reading source code.
    IOError(std::io::Error),
    /// Source code is not valid UTF-8.
    NotUTF8,
}

impl Display for InputError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            InputError::IOError(e) => (e as &dyn Display).fmt(f),
            InputError::NotUTF8 => write!(f, "INPUT ERROR: Source is not UTF-8"),
        }
    }
}

impl Error for InputError {}

impl From<std::io::Error> for InputError {
    fn from(e: std::io::Error) -> Self {
        Self::IOError(e)
    }
}

/// Index pointing into the source.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SourceIndex {
    /// Line index of the character.
    pub line: usize,
    /// Column index of the character.
    pub char: usize,
}

impl SourceIndex {
    pub fn new(line: usize, char: usize) -> Self {
        Self { line, char }
    }

    pub fn span(
        (from_line, from_char): (usize, usize),
        (to_line, to_char): (usize, usize),
    ) -> Range<Self> {
        Self::new(from_line, from_char)..Self::new(to_line, to_char)
    }

    pub fn span_one(self) -> Range<Self> {
        let mut end = self;
        end.char += 1;
        self..end
    }

    pub fn next(mut self) -> Self {
        self.char += 1;
        self
    }

    pub fn span_empty() -> Range<Self> {
        Self::span((0, 0), (0, 0))
    }
}

/// An error associated with a part of source code.
#[derive(Debug)]
pub struct SourceError {
    span: Range<SourceIndex>,
    message: String,
}

impl SourceError {
    pub fn new(span: Range<SourceIndex>, message: String) -> Self {
        Self { span, message }
    }

    /// Returns a string containing the error message and
    /// the highlighted part of code that caused the error.
    pub fn display(&self, src: &Source) -> String {
        let mut msg = String::new();
        msg.push_str(&format!("{}\n\n", self.message));
        if self.span.start != self.span.end {
            let highlight = if self.span.start.line == self.span.end.line {
                highlight_oneline(
                    src,
                    self.span.start.line,
                    self.span.start.char..self.span.end.char,
                )
            } else {
                highlight_multiline(src, &self.span)
            };
            msg.push_str(&highlight);
        }
        msg
    }

    /// Returns a string containing the error message and
    /// the source index span it refers to.
    /// Use when Source is not available.
    pub fn display_no_src(&self, input_name: &str) -> String {
        if self.span.start.line != self.span.end.line
            || self.span.start.char + 1 < self.span.end.char
        {
            format!(
                "[{}] from {}:{} to {}:{}\n{}\n\n",
                input_name,
                self.span.start.line,
                self.span.start.char,
                self.span.end.line,
                self.span.end.char,
                self.message,
            )
        } else if self.span.start.char + 1 == self.span.end.char {
            format!(
                "[{}] at {}:{}\n{}\n\n",
                input_name, self.span.start.line, self.span.start.char, self.message,
            )
        } else {
            format!("[{}]\n{}\n\n", input_name, self.message,)
        }
    }
}

/// Make the code highlight part for a single-line source error.
fn highlight_oneline(src: &Source, line: usize, char_span: Range<usize>) -> String {
    let mut msg = String::new();
    let indent = format!("{} |", line).len();
    msg.push_str(&format!("{} |{}", line + 1, src.lines[line]));
    msg.push_str(&format!(
        "{}\n\n",
        " ".repeat(indent - 1)
            .chars()
            .chain("|".chars())
            .chain(" ".repeat(char_span.start).chars())
            .chain("^".repeat(char_span.end - char_span.start).chars())
            .collect::<String>(),
    ));
    msg
}

/// Make the code highlight part for a multi-line source error.
fn highlight_multiline(src: &Source, span: &Range<SourceIndex>) -> String {
    let mut msg = String::new();
    let indent = count_indent(span);
    let first_line = span.start.line;
    let last_line = span.end.line;
    // first line
    msg.push_str(&format!(
        "{:<width$}|{}",
        first_line + 1,
        src.lines[first_line],
        width = indent - 1,
    ));
    msg.push_str(&format!(
        "{}\n",
        " ".repeat(indent - 1)
            .chars()
            .chain("|".chars())
            .chain(" ".repeat(span.start.char).chars())
            .chain(
                "^".repeat(src.lines[first_line].len() - span.start.char)
                    .chars()
            )
            .collect::<String>(),
    ));
    // middle lines
    for line in first_line + 1..last_line {
        msg.push_str(&format!(
            "{:<width$}|{}",
            line + 1,
            src.lines[line],
            width = indent - 1,
        ));
        msg.push_str(&format!(
            "{}\n",
            " ".repeat(indent - 1)
                .chars()
                .chain("|".chars())
                .chain("^".repeat(src.lines[line].len()).chars())
                .collect::<String>(),
        ));
    }
    // last line
    msg.push_str(&format!(
        "{:<width$}|{}",
        last_line + 1,
        src.lines[last_line],
        width = indent - 1,
    ));
    msg.push_str(&format!(
        "{}\n\n",
        " ".repeat(indent - 1)
            .chars()
            .chain("|".chars())
            .chain("^".repeat(span.end.char).chars())
            .collect::<String>(),
    ));
    msg
}

/// Count the indent of the source code printing for multi-line source errors.
fn count_indent(span: &Range<SourceIndex>) -> usize {
    format!("{} |", span.start.line)
        .len()
        .max(format!("{} |", span.end.line).len())
}

#[cfg(test)]
mod test;
