use crate::source::{Source, SourceError, SourceIndex, SourceIter, SourceIterItem};

#[test]
fn source_iter_indices() {
    let src = "ab\ncd";
    let source_iter = SourceIter::new(src.as_bytes());
    let iter_collect = source_iter.collect::<Vec<_>>();
    let expected = vec![
        SourceIterItem {
            char: 'a',
            index: SourceIndex::new(0, 0),
        },
        SourceIterItem {
            char: 'b',
            index: SourceIndex::new(0, 1),
        },
        SourceIterItem {
            char: '\n',
            index: SourceIndex::new(0, 2),
        },
        SourceIterItem {
            char: 'c',
            index: SourceIndex::new(1, 0),
        },
        SourceIterItem {
            char: 'd',
            index: SourceIndex::new(1, 1),
        },
    ];

    for index in 0..iter_collect.len().max(expected.len()) {
        assert_eq!(iter_collect[index], expected[index]);
    }
}

#[test]
fn source_iter_content() {
    let src = "\
let x = 1;
let y = x + 1;
";
    let source_iter = SourceIter::new(src.as_bytes());
    let src_collect = source_iter.map(|sii| sii.char).collect::<String>();
    assert_eq!(src, src_collect);
}

#[test]
fn noline_source_error_display() {
    let src = "\
fn a () {
    let a = 1;
}
";
    let source = Source::new(src.as_bytes()).unwrap();
    let msg = "No main function definition.";
    let expected = format!(
        "\
{}

",
        msg,
    );
    let error = SourceError::new(SourceIndex::span_empty(), msg.to_string());
    let error_msg = error.display(&source);
    assert_eq!(expected, error_msg);
}

#[test]
fn oneline_source_error_display() {
    let src = "\
let x = 1;
let y = asdf$heh;
let z = 3;
";
    let source = Source::new(src.as_bytes()).unwrap();
    let msg = "Tokenizer error - Unexpected character: $";
    let expected = format!(
        "\
{}

2 |let y = asdf$heh;
  |            ^

",
        msg
    );
    let error = SourceError::new(SourceIndex::span((1, 12), (1, 13)), msg.to_string());
    let error_msg = error.display(&source);

    assert_eq!(&expected, &error_msg);
}

#[test]
fn multiline_source_error_display() {
    let src = "\
let x = 1;
let y: String = 2 + 3 +
    4 + 5 + 6
    + 7; // comment
let z = 3;
";
    let source = Source::new(src.as_bytes()).unwrap();
    let msg = "Type error - expected String, found Int";
    let expected = format!(
        "\
{}

2 |let y: String = 2 + 3 +
  |                ^^^^^^^^
3 |    4 + 5 + 6
  |^^^^^^^^^^^^^^
4 |    + 7; // comment
  |^^^^^^^

",
        msg
    );
    let error = SourceError::new(SourceIndex::span((1, 16), (3, 7)), msg.to_string());
    let error_msg = error.display(&source);

    assert_eq!(&expected, &error_msg);
}

#[test]
fn source_error_display_no_src_span_zero() {
    let msg = "error message";
    let error = SourceError::new(SourceIndex::span((0, 2), (0, 2)), msg.to_string());
    let expected = format!(
        "\
[input]
{}

",
        msg
    );
    let error_msg = error.display_no_src("input");

    assert_eq!(&expected, &error_msg);
}

#[test]
fn source_error_display_no_src_span_one() {
    let msg = "error message";
    let error = SourceError::new(SourceIndex::span((0, 2), (0, 3)), msg.to_string());
    let expected = format!(
        "\
[input] at 0:2
{}

",
        msg
    );
    let error_msg = error.display_no_src("input");

    assert_eq!(&expected, &error_msg);
}

#[test]
fn source_error_display_no_src_span_multiple() {
    let msg = "error message";
    let error = SourceError::new(SourceIndex::span((0, 2), (2, 13)), msg.to_string());
    let expected = format!(
        "\
[input] from 0:2 to 2:13
{}

",
        msg
    );
    let error_msg = error.display_no_src("input");

    assert_eq!(&expected, &error_msg);
}
